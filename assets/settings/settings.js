$(function() {
    $("[data-moveto]").each( function() {
        var moveTo = $(this).data('moveto');
        $(this).appendTo($("#"+moveTo));
    });
    $("[data-click-name]").on("click", function() {
        event.preventDefault();
        $("button[name='" + $(this).data("click-name") + "'][value='" + $(this).data("click-value") +"']").first().trigger("click");
    });
});
