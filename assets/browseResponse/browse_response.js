/* jshint esversion: 6 */
/* @version 5.15.1 */
$(document).ajaxComplete(function() {
    if($('#responses-grid table').data('reloadAnyResponse') == 'done') {
        return;
    }
    $('.ri-pencil-fill.text-success').each(function( index, editlink) {
        let dropdownmenu = $(editlink).closest('.dropdown-menu');
        if ($(dropdownmenu).data('reloadAnyResponse') == 'done') {
            return;
        }
        let dropdownid = $(dropdownmenu).attr('id').replace('dropdownmenu_','');
        let dropdown = $('#dropdown_' + dropdownid);
        if ($(dropdown).length) {
            let srid = $(dropdown).closest('tr').find('input').eq(0).val();
            let token = "";
            if ($(dropdown).closest('tr').find('a.btn.edit-token').length) {
                token = '&token=' + $(element).find('a.btn.edit-token').text().trim();
            }
            let newLinkHtml = '<a class="dropdown-item" href="' + LS.reloadAnyResponse.baseUrl + srid + token +'" target="_blank">' +
                '<i class="ri-play-fill text-success" aria-hidden="true"></i>' + LS.reloadAnyResponse.lang.launch +
                '</a>';
            $(editlink).closest('li').after('<li><div>' + newLinkHtml + '</div></li>');
        }
        $(dropdownmenu).data('reloadAnyResponse','done');
    });
    $('#responses-grid table').data('reloadAnyResponse','done');
});
