$(document).ajaxComplete(function() {
    if($('#responses-grid table').data('reloadAnyResponse') == 'done') {
        return;
    }

    $('#responses-grid table tbody').find('tr').each(function( index, element) {
        let pencil = $(element).find('.button-column,.icon-btn-row').find('a .fa-pencil');
        if($(pencil).length) {
            let srid = $(element).find('input').eq(0).val();
            let token = "";
            if ($(element).find('a.edit-token').length) {
                token = '&token=' + $(element).find('a.edit-token').clone().children().remove().end().text().trim();
            }
            let newLink = $('<a/>', {
                'class': LS.reloadAnyResponse.lsVersion >= 4 ? 'btn btn-default btn-sm' : 'btn btn-default btn-xs',
                'href': LS.reloadAnyResponse.baseUrl + srid + token,
                'target': '_blank',
                'title': LS.reloadAnyResponse.lang.launch,
                'data-toggle': 'tooltip'
            }).html('<i class="fa fa-cog text-success" aria-hidden="true"></i>');
            $(pencil).closest("a").after(newLink);
        }
    });
    $('#responses-grid table').data('reloadAnyResponse','done');
});
