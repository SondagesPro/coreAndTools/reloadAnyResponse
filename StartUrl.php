<?php

/**
 * Some Utilities
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2020-2023 Denis Chenu <http://www.sondages.pro>
 * @license AGPL v3
 * @version 5.15.6
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

namespace reloadAnyResponse;

use App;
use Yii;
use CDbCriteria;
use CHttpException;
use Survey;
use Response;
use Permission;

class StartUrl
{
    /* var null|integer $surveyId */
    private $surveyId;
    /* var null|string $token */
    private $token;
    /* var boolan $usetoken */
    private $usetoken = true ;

    /* var boolean is available */
    private $available = false;
    /* var boolean survey token allowed */
    private $surveytokenavailable = false;
    /* var array settings */
    private $currentSettings = array();

    /**
     * constructor
     * @param integer survey id
     * @param string token
     * @throw Exception
     */
    public function __construct($surveyId, $token = null, $usetoken = true)
    {
        $oSurvey = \Survey::model()->findByPk($surveyId);
        if (empty($oSurvey)) {
            throw new \Exception(404, gT("The survey in which you are trying to participate does not seem to exist."));
        }
        $this->surveyId = $surveyId;
        $this->surveytokenavailable = !$oSurvey->getIsAnonymized() && $oSurvey->getHasTokensTable();
        $this->token = $token;
        $this->usetoken = $usetoken;
    }

    /**
     * Check survey with current information,
     * Didn't check if code exist or permission
     * @return boolean
     */
    public function isAvailable()
    {
        return Utilities::SurveyIsValid($this->surveyId);
    }

    /**
     * Get the url for a srid
     * @param integer needed srid
     * @param string[] $extraParams to be added on url
     * @param boolean $createCode create code (if not exist)
     * @param boolean $absolute url
     * @param boolean $checkadminright
     * @param string|null $accesscode if set try to use this access code
     * return false|string
     */
    public function getUrl($srid, $extraParams = array(), $createCode = false, $absolute = false, $checkadminright = true, $accesscode = null)
    {
        if (!$this->isAvailable()) {
            return false;
        }
        if (empty($srid)) {
            \Yii::log("Call of getUrl without srid.", \CLogger::LEVEL_ERROR, 'plugin.reloadAnyResponse.StartUrl.getUrl');
            return;
        }
        $params = array(
            'sid' => $this->surveyId,
            'srid' => $srid,
            'lang' => App()->getLanguage()
        );
        $select = array('id');
        if ($this->token) {
            if ($this->surveytokenavailable && $this->usetoken) {
                $params['token'] = $this->token;
                $select[] = 'token';
            }
        }
        $params = array_merge($params, $extraParams);
        /* Get the response information  Need only token and srid, response can be big */
        $responseCriteria = Utilities::getResponseCriteria($this->surveyId, $srid);
        $responseCriteria->select = $select;
        $oResponse = \Response::model($this->surveyId)->find($responseCriteria);
        if (!$oResponse) {
            return false;
        }
        /* Plugin event to disable access*/
        if (Utilities::PluginEventEditDisabled($this->surveyId, $srid, $this->token, 'getUrl')) {
            return false;
        }
        /* Check if accesscode ? Before token ? : review process ? */
        /* Create the specific admin right */
        $haveAdminRight = false;
        if ($checkadminright && Permission::model()->hasSurveyPermission($this->surveyId, 'responses', 'update') && $this->getSetting('allowAdminUser')) {
            $haveAdminRight = true;
        }
        if ($haveAdminRight) {
            if (Utilities::AdminAllowEdit($this->surveyId, $srid)) {
                /* Add token if needed and possible */
                if ($this->surveytokenavailable) {
                    if (empty($this->token) || !Utilities::checkIsValidToken($this->surveyId, $this->token)) {
                        if (!empty($oResponse->token) && Utilities::checkIsValidToken($this->surveyId, $oResponse->token)) {
                            $params['token'] = $oResponse->token;
                        }
                    }
                }
                if ($absolute) {
                    return App()->createAbsoluteUrl("survey/index", $params);
                }
                return App()->createUrl("survey/index", $params);
            }
        }

        /* Check token validaty according to current srid */
        if ($this->surveytokenavailable && $this->usetoken) {
            $allowTokenUser = $this->getSetting('allowTokenUser');
            if (!$allowTokenUser) {
                /* Check for default LimeSurvey behaviour */
                $oSurvey = Survey::model()->findByPk($this->surveyId);
                if ($oSurvey->isTokenAnswersPersistence) {
                    /* Same than defauilt LimeSurvey behaviour */
                    $oResponse = \Response::model($this->surveyId)->findByAttributes(
                        array('token' => $this->token),
                        array('order' => 'id DESC')
                    );
                    if ($oResponse && $oResponse->id == $srid && (empty($oResponse->submitdate) || $oSurvey->isAllowEditAfterCompletion)) {
                        $allowTokenUser = true;
                    }
                }
            }
            if ($allowTokenUser) {
                if (Utilities::TokenAllowEdit($this->surveyId, $srid, $this->token)) {
                    // Same token can return true
                    if ($absolute) {
                        return App()->createAbsoluteUrl("survey/index", $params);
                    }
                    return App()->createUrl("survey/index", $params);
                }
            }

            // Must continue if have accesscode
        }
        /* Check access code */
        $uniqueCodeAccess = $this->getSetting('uniqueCodeAccess');
        if ($uniqueCodeAccess && Utilities::CheckAccessCodeAllowEdit($this->surveyId, $srid)) {
            $responseLink = \reloadAnyResponse\models\responseLink::model()->findByPk(['sid' => $this->surveyId, 'srid' => $srid]);
            if ($responseLink && $responseLink->accesscode) {
                if (!Utilities::AccessCodeAllowEdit($this->surveyId, $srid, $responseLink->accesscode)) {
                    return false;
                }
                if ($accesscode && $accesscode != $responseLink->accesscode) {
                    return false;
                }
                $params['code'] = $responseLink->accesscode;
                if ($absolute) {
                    return App()->createAbsoluteUrl("survey/index", $params);
                }
                return App()->createUrl("survey/index", $params);
            }
            if ($createCode || $this->getSetting('uniqueCodeCreate')) {
                $responseLink = \reloadAnyResponse\models\responseLink::model()->setResponseLink($this->surveyId, $srid, $this->token);
                if ($responseLink && $accesscode) {
                    $responseLink->accesscode = $accesscode;
                    $responseLink->save();
                }
                if ($responseLink && $responseLink->accesscode) {
                    $params['code'] = $responseLink->accesscode;
                    if ($absolute) {
                        return App()->createAbsoluteUrl("survey/index", $params);
                    }
                    return App()->createUrl("survey/index", $params);
                }
            }
        }
        /* No other right : plugin event */
        if (Utilities::PluginEventEditAllowed($this->surveyId, $srid, $this->token, 'getUrl')) {
            if ($absolute) {
                return App()->createAbsoluteUrl("survey/index", $params);
            }
            return App()->createUrl("survey/index", $params);
        }
        return false;
    }

    /**
     * get setting of the plugin
     * @see reloadAnyResponse\getSetting
     * @return mixed
     */
    public function getSetting($setting)
    {
        if (isset($this->settings[$setting])) {
            return $this->settings[$setting];
        }
        $this->settings[$setting] = Utilities::getReloadAnyResponseSetting($this->surveyId, $setting);
        return $this->settings[$setting];
    }
}
