<?php

/**
 * Token helper system for reloadAnyResponse
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2022-2023 Denis Chenu <http://www.sondages.pro>
 * @license AGPL v3
 * @version 5.9.4
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

namespace reloadAnyResponse;

use App;
use Yii;
use Survey;
use SurveyDynamic;
use Response;

class TokenReloadInstance
{
    /**
     * Singleton
     * @var self
     */
    private static $instance = null;

    /**
     * @var integer|null survey id
     */
    private $surveyid;

    /**
     * @var boolean validsurvey
     */
    private $validsurvey;

    /**
     * @var boolean validsurvey
     */
    private $tokensurvey;

    /**
     * @var string|null token
     */
    private $token;

    /**
     * @var boolean tokeneditable
     */
    private $tokeneditable;

    /**
     * @var boolean tokeneditable
     */
    private $tokenmanager;

    /**
     * @var null|array() tokenlist
     */
    private $relatedtokenList;

    /**
     * @var null|array() sridlist allowed
     */
    private $allowedSridList;


    /**
     * constructor
     * @param integer survey id
     * @param string token
     */
    public function __construct($surveyid, $token)
    {
        $this->surveyid = $surveyid;
        $this->token = $token;
    }

    /**
     * Get the singleton
     * @param integer survey id
     * @param string token
     * return self
     */
    public static function getInstance($surveyid, $token)
    {
        if ((null === self::$instance) || ($surveyid !== self::$instance->surveyid) || ($token !== self::$instance->token)) {
            self::$instance = new self($surveyid, $token);
        }
        return self::$instance;
    }

    /**
     * Check if a response can be loaded by a particular token
     * @param integer $srid
     * @return boolean
     */
    public function getTokenAllowEdit($srid)
    {
        if (!$this->isValidSurvey()) {
            return false;
        }
        $tokenManaged = version_compare(App()->getConfig('TokenUsersListAndManageAPI'), '0.14', ">=");
        if (!Utilities::getReloadAnyResponseSetting($this->surveyid, 'allowTokenUser') && !$tokenManaged) {
            return false;
        }
        $responseCriteria = Utilities::getResponseCriteria($this->surveyid, $srid);
        $responseCriteria->select = array('id', 'submitdate', 'token');
        $oResponse = SurveyDynamic::model($this->surveyid)->find($responseCriteria);
        if (empty($oResponse) || empty($oResponse->token)) {
            return false;
        }
        $isValidToken = self::getIsValidToken($oResponse->token);
        if (!$isValidToken) {
            return false;
        }
        /* Token is in same group than response token */
        if ($extraFilters = trim(Utilities::getReloadAnyResponseSetting($this->surveyid, 'extraRestrictionToken'))) {
            $criteria = Utilities::getResponseCriteria($this->surveyid, $srid, $extraFilters);
            $oResponseCount = intval(SurveyDynamic::model($this->surveyid)->count($criteria));
            if (!$oResponseCount) {
                return false;
            }
        }
        if (is_null($this->tokenmanager)) {
            $this->tokenmanager = false; // If not $tokenManaged : no need to check if manager
            if ($tokenManaged) {
                $tokenTableId = \TokenUsersListAndManagePlugin\Utilities::getSurveyHasTokenTable($this->surveyid);
                if ($tokenTableId) {
                    \Yii::import('TokenUsersListAndManagePlugin.models.TokenManaged');
                    $TokenManaged = \TokenManaged::model($tokenTableId)->findByToken($this->token);
                    if ($TokenManaged && $TokenManaged->getIsManager()) {
                        $this->tokenmanager = true;
                    }
                }
            }
        }
        $isManager = $this->tokenmanager;
        if ($tokenManaged && !empty($oResponse->submitdate)) {
            $allowSubmitted = Utilities::getReloadAnyResponseSetting($this->surveyid, 'allowUserOnSubmitted');
            if (!$allowSubmitted) {
                if (!$isManager) {
                    return false;
                }
                if (!Utilities::getReloadAnyResponseSetting($this->surveyid, 'allowTokenGroupManagerOnSubmitted')) {
                    return false;
                }
            }
        }
        if ($this->token == $oResponse->token) {
            if (Utilities::getReloadAnyResponseSetting($this->surveyid, 'allowTokenUser')) {
                return true;
            }
            if ($isManager && Utilities::getReloadAnyResponseSetting($this->surveyid, 'allowTokenGroupManager')) {
                return true;
            }
            /* Check for default LimeSurvey behaviour */
            $oSurvey = Survey::model()->findByPk($this->surveyid);
            if ($oSurvey->isTokenAnswersPersistence) {
                /* Same than defauilt LimeSurvey behaviour */
                $oResponse = Response::model($this->surveyid)->findByAttributes(
                    array('token' => $this->token),
                    array('order' => 'id DESC')
                );
                if ($oResponse && $oResponse->id == $srid && (empty($oResponse->submitdate) || $oSurvey->isAllowEditAfterCompletion)) {
                    return true;
                }
            }
            return false;
        }
        if (!Utilities::getReloadAnyResponseSetting($this->surveyid, 'allowTokenGroupUser')) {
            if (!$isManager) {
                return false;
            }
            if (!Utilities::getReloadAnyResponseSetting($this->surveyid, 'allowTokenGroupManager')) {
                return false;
            }
        }
        if ($tokenManaged) {
            if (is_null($this->allowedSridList)) {
                $this->allowedSridList = \TokenUsersListAndManagePlugin\Utilities::getAllowedSridList($this->surveyid, $this->token);
                if (is_null($this->allowedSridList)) {
                    $this->allowedSridList = false;
                }
            }
            /* getAllowedSridList return false or null if not checked, array if checked */
            if (is_array($this->allowedSridList) && !in_array($oResponse->id, $this->allowedSridList)) {
                return false;
            }
        }
        return true;
    }

    /**
     * get if current survey is valid
     * @retuen boolean
     */
    private function isValidSurvey()
    {
        if (!is_null($this->validsurvey)) {
            return $this->validsurvey;
        }
        if (empty(Survey::model()->findByPk($this->surveyid))) {
            $this->validsurvey = false;
            return false;
        }
        $oSurvey = Survey::model()->findByPk($this->surveyid);
        $this->validsurvey = $oSurvey->getState() == 'running' && !$oSurvey->getIsAnonymized();
        return $this->validsurvey;
    }

    /**
     * get if current survey have tokens
     * @retuen boolean
     */
    private function isTokenSurvey()
    {
        if (!is_null($this->tokensurvey)) {
            return $this->tokensurvey;
        }
        if (!$this->isValidSurvey()) {
            return $this->validsurvey;
        }
        $this->tokensurvey = Survey::model()->findByPk($this->surveyid)->getHasTokensTable();
        return $this->tokensurvey;
    }

    /**
     * Check if current token is valid
     * @param string $token for control
     * @return boolean
     */
    public function getIsValidToken($validtoken = null)
    {
        if (empty($this->token)) {
            return false;
        }
        if (is_null($this->tokeneditable)) {
            $this->tokeneditable = true;
            if ($this->isTokenSurvey()) {
                $oToken = \Token::model($this->surveyid)->editable()->findByAttributes(array('token' => $this->token));
                /* Todo : get it from parent*/
                if (empty($oToken)) {
                    $this->tokeneditable = false;
                } elseif (version_compare(App()->getConfig('TokenUsersListAndManageAPI'), '0.14', ">=")) {
                    if ($oToken->emailstatus == 'disable') {
                        $this->tokeneditable = false;
                    }
                }
            }
        }
        if (!$this->tokeneditable) {
            return false;
        }
        if (empty($validtoken)) {
            return true;
        }
        if ($this->token == $validtoken) {
            return true;
        }
        if (in_array($validtoken, $this->getRelatedTokens())) {
            return true;
        }
        return false;
    }

    /**
     * get list of related tokens
     * @return string[]
     */
    public function getRelatedTokens()
    {
        if (!is_null($this->relatedtokenList)) {
            return $this->relatedtokenList;
        }
        $this->relatedtokenList = [$this->token => $this->token];
        if (version_compare(App()->getConfig('TokenUsersListAndManageAPI'), '0.14', ">=")) {
            $this->relatedtokenList = \TokenUsersListAndManagePlugin\Utilities::getTokensList($this->surveyid, $this->token, false);
        } elseif (Yii::getPathOfAlias('responseListAndManage')) {
            $this->relatedtokenList = \responseListAndManage\helpers\tokensList::getTokensList($this->surveyid, $this->token);
        }
        return $this->relatedtokenList;
    }
}
