<?php

/**
 * Plugin helper for limesurvey : new class and function allowing to reload any survey
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2018-2025 Denis Chenu <http://www.sondages.pro>
 * @license AGPL v3
 * @version 5.15.6
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

class reloadAnyResponse extends PluginBase
{
    protected $storage = 'DbStorage';

    protected static $description = 'New class and function allowing to reload any survey.';
    protected static $name = 'reloadAnyResponse';

    protected static $dbVersion = 6;

    /* @var null|interger Keep reload srid during all events */
    private $reloadedSrid = null;
    /* @var null|interger Keep current srid during all events */
    private $currentSrid = null;
    /* @var null|string Keep current page during all page produced */
    private static $pageId = null;
    /* @var null|string Keep reload token during all events */
    private $reloadedToken = null;
    /* @var null|string original token if need to be resetted */
    private $originalToken = null;

    /* @var null|interger Keep current surveyid */
    private $surveyId = null;

    /* @var boolean need to fix alloweditaftercompletion */
    private $enablealloweditaftercompletion = false;
    /* @var boolean need to fix tokenanswerspersistence */
    private $disabletokenanswerspersistence = false;

    /**
     * @var array[] the settings
     */
    protected $settings = array(
        'information' => array(
            'type' => 'info',
            'content' => 'The default settings for all surveys. Remind no system is set in this plugin to show or send link to user.',
        ),
        'allowAdminUser' => array(
            'type' => 'checkbox',
            'htmlOptions' => array(
                'value' => 1,
                'uncheckValue' => 0,
            ),
            'label' => "Allow admin user to reload any survey with response id.",
            'default' => 1,
        ),
        'replaceEditResponse' => array(
            'type' => 'checkbox',
            'htmlOptions' => array(
                'value' => 1,
                'uncheckValue' => 0,
            ),
            'label' => "Replace edit response in browse response interface.",
            'help' => "When an admin user want to edit an existing response directly redirect to editing the public survey.",
            'default' => 0,
        ),
        'addEditResponseOnBrowse' => array(
            'type' => 'checkbox',
            'htmlOptions' => array(
                'value' => 1,
                'uncheckValue' => 0,
            ),
            'label' => "Add an edit button on browser.",
            'help' => "Add a button on browse reponse if user is allowed to edit (only if replace edit response is not activated).",
            'default' => 1,
        ),
        'allowTokenUser' => array(
            'type' => 'select',
            'options' => array(
                1 => "Yes",
                0 => "No",
            ),
            'label' => "Allow user with a valid token.",
            'default' => 1,
        ),
        'allowTokenGroupUser' => array(
            'type' => 'select',
            'options' => array(
                1 => "Yes",
                0 => "No",
            ),
            'label' => "Allow user with a token in same group.",
            'help' => "Group is related to User group management from responseListAndManage plugin",
            'default' => 1,
        ),
        'uniqueCodeCreate' => array(
            'type' => 'checkbox',
            'htmlOptions' => array(
                'value' => 1,
                'uncheckValue' => 0,
            ),
            'label' => "Create automatically unique code for all surveys.",
            'help' => "If code exist, it can be always used.",
            'default' => 0,
        ),
        'uniqueCodeAccess' => array(
            'type' => 'checkbox',
            'htmlOptions' => array(
                'value' => 1,
                'uncheckValue' => 0,
            ),
            'label' => "Allow entering unique code for all surveys if exist.",
            'help' => "If you set to no, this disable usage for other plugins.",
            'default' => 1,
        ),
        'useDeletedManaged' => array(
            'type' => 'checkbox',
            'htmlOptions' => array(
                'value' => 1,
                'uncheckValue' => 0,
            ),
            'label' => "Use filter from deletion from RelatedSureyManagement",
            'default' => 1,
        ),
        'deleteLinkWhenResponseDeleted' => array(
            'type' => 'checkbox',
            'htmlOptions' => array(
                'value' => 1,
                'uncheckValue' => 0,
            ),
            'label' => "Delete the link of response when a response is deleted.",
            'help' => "This delete the response link when response is deleted. If you use VV import, don't activate this option.",
            'default' => 0,
        ),
        'deleteLinkWhenSurveyDeactivated' => array(
            'type' => 'checkbox',
            'htmlOptions' => array(
                'value' => 1,
                'uncheckValue' => 0,
            ),
            'label' => "Delete the link of all responses of a survey when it's deactivated.",
            'help' => "Since response table keep the current auto increment value, leave this option can not broke response security.",
            'default' => 0,
        ),
        'deleteLinkWhenSurveyDeleted' => array(
            'type' => 'checkbox',
            'htmlOptions' => array(
                'value' => 1,
                'uncheckValue' => 0,
            ),
            'label' => "Delete the link of all responses of a survey when it's deleted.",
            'help' => "To avoid a big table.",
            'default' => 1,
        ),
        'disableMultiAccess' => array(
            'type' => 'info',
            'content' => "<div class='alert alert-info'>You need renderMessage for disabling multiple access.</div>",
            'default' => 1,
        ),
        'multiAccessTime' => array(
            'type' => 'int',
            'label' => 'Time for disable multiple access (in minutes) (config.php settings replace it of not exist)',
            'help' => 'Before save value or entering survey : test if someone else edit response in this last minutes. Disable save and show a message if yes. Set to empty disable system but then answer of user can be deleted by another user without any information…',
            'htmlOptions' => array(
                'min' => 1,
                'placeholder' => 'Disable',
            ),
            'default' => '',
        ),
        'throwErrorRight' => array(
            'type' => 'checkbox',
            'htmlOptions' => array(
                'value' => 1,
                'uncheckValue' => 0,
            ),
            'label' => 'Throw error when try to edit a response without right (Default)',
            'help' => 'Send an http 401 error when srid is in url, but user did ,not have right. Else create a new response (according to survey settings) . ',
            'default' => 0,
        ),
        'replaceDefaultSave' => array(
            'type' => 'select',
            'options' => array(
                0 => "Never",
                1 => "When survey is reloaded by access code or token.",
                2 => "Always, each time survey is reloaded.",
            ),
            'label' => 'Replace the default LimeSurvey system if survey reloaded',
            'help' => 'When participant try to save a reloaded reponse : it was directly saved without showing the save form.',
            'default' => 1,
        ),
        'clearAllAction' => array(
            'type' => 'select',
            'options' => array(
                'reset' => "Reset session, don‘t delete current response",
                'partial' => "Reset session, delete current response if was not submitted.",
                'all' => "Reset session, delete current response in any condition.",
            ),
            'label' => "Action when using clearall action.",
            'help' => "Action to do when participant want to clear all. Default action by LimeSurvey was to delete not submitted response.",
            'default' => 'partial',
        ),
        'clearAllActionForced' => array(
            'type' => 'checkbox',
            'htmlOptions' => array(
                'value' => 1,
                'uncheckValue' => 0,
            ),
            'label' => "Replace clearall action even if response was not reloaded",
            'default' => 0,
        ),
        'clearAllActionReloadSrid' => array(
            'type' => 'checkbox',
            'htmlOptions' => array(
                'value' => 1,
                'uncheckValue' => 0,
            ),
            'label' => "Add the current response id in the restart link after clearall",
            'default' => 0,
        ),
        'clearAllActionManaged' => array(
            'type' => 'checkbox',
            'htmlOptions' => array(
                'value' => 1,
                'uncheckValue' => 0,
            ),
            'label' => "If clearall are managed by RelatedSurveyManagement : always use it",
            'default' => 0,
        ),
        'reloadResetSubmitted' => array(
            'type' => 'checkbox',
            'htmlOptions' => array(
                'value' => 1,
                'uncheckValue' => 0,
            ),
            'label' => "When survey is save or move for the 1st time : reset it as not submitted.",
            'default' => 1,
        ),
        'keepMaxStep' => array(
            'type' => 'checkbox',
            'htmlOptions' => array(
                'value' => 1,
                'uncheckValue' => 0,
            ),
            'label' => "Keep max step during each load and save.",
            'help' => "Warning : when set this by default : it update all current survey table",
            'default' => 0,
        ),
        /* surveySession settings */
        'noHttpUserAgent' => array(
            'type' => 'select',
            'options' => array(
                'forbidden' => 'Disable access with a 403',
                'action' => 'Allow access and block other access',
            ),
            'htmlOptions' => array(
                'empty' => 'Allow access but don‘t block other access',
            ),
            'label' => 'Action to do without HTTP_USER_AGENT',
            'help' => 'By default : show the page but don\'t disable other access. You can choose to send a http 403 error (Forbidden) . ',
            'default' => '',
        ),
        'botRegexp' => array(
            'type' => 'string',
            'htmlOptions' => array(
                'placeholder' => '/bot|crawl|slurp|spider|mediapartners|lua-resty-http/i',
            ),
            'label' => 'Bot regexp',
            'help' => 'Usage of preg_match : you can add new bot or invalid request with “|newbot”.',
            'default' => '',
        ),
        'botHttpUserAgent' => array(
            'type' => 'select',
            'options' => array(
                'forbidden' => 'Disable access with a 403',
                'action' => 'Allow access and block other access',
            ),
            'htmlOptions' => array(
                'empty' => 'Allow access but don‘t block other access',
            ),
            'label' => 'Action to do with bot',
            'help' => 'By default : show the page but don\'t disable other access. You can choose to send a http 403 error (Forbidden) . ',
            'default' => '',
        ),
    );

    /** @inheritdoc **/
    public function init()
    {
        /* Set path of Alias at start */
        Yii::setPathOfAlias(get_class($this), dirname(__FILE__));

        /* Set the config */
        $this->subscribe('afterPluginLoad', 'setConfigInAfterPluginLoad');

        if (Yii::app() instanceof CConsoleApplication) {
            return;
        }

        /* Managing unique code for Response and SurveyDynamic */
        $this->subscribe('afterModelSave');
        $this->subscribe('afterModelDelete');
        /* Delete related link for Survey */
        $this->subscribe('afterSurveySave');
        $this->subscribe('afterSurveyDelete');
        $this->subscribe('beforeSurveyDeleteMany');

        /* Get the survey by srid and code */
        /* Save current session */
        $this->subscribe('beforeSurveyPage');
        /* Some needed action when srid is set */
        $this->subscribe('beforeTwigRenderTemplate');
        /* Replace existing system if srid = new */
        $this->subscribe('beforeLoadResponse');
        /* Add a checker when multiple tab is open */
        $this->subscribe('beforeQuestionRender');

        /* The survey setting in TULAM */
        $this->subscribe('beforeTULAMSurveySettings');
        $this->subscribe('newTULAMSurveySettings');

        /* delete current session*/
        $this->subscribe("afterSurveyComplete", 'afterSurveyDeleteSurveySession');
        $this->subscribe("afterSurveyQuota", 'afterSurveyDeleteSurveySession');
        /* delete current session when unload */
        $this->subscribe("newDirectRequest", 'newDirectRequest');
        /* redirect to editing survey */
        $this->subscribe("beforeControllerAction", 'beforeControllerAction');

        /* Logout remove all session, survey too, then must delete current related surveySessionId */
        $this->subscribe("beforeLogout", "deleteAllBySessionId");

        /* In tool */
        $this->subscribe('beforeToolsMenuRender');
    }

    /**
     * Delete all related current survey session from this user
     * @return @void
     */
    public function deleteAllBySessionId()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        \reloadAnyResponse\models\surveySession::deleteAllBySessionId();
    }

    /** @inheritdoc **/
    public function getPluginSettings($getValues = true)
    {
        if (!Permission::model()->hasGlobalPermission('settings', 'read')) {
            throw new CHttpException(403);
        }
        /* @todo translation of label and help */
        $pluginSettings = parent::getPluginSettings($getValues);
        if (version_compare(App()->getConfig("versionnumber"), "3", "<")) {
            return $pluginSettings;
        }
        $oSurveymenuManage = SurveymenuEntries::model()->find("name = :name", array(":name" => 'ReloadAnyResponseSettings'));
        $state = !empty($oSurveymenuManage);
        $help = $state ? $this->translate('Menu exist, to delete : uncheck box and validate.') : $this->translate("Settings menu didn‘t exist, to create check box and validate.");
        $pluginSettings['createReloadAnyResponseSettings'] = array(
            'type' => 'checkbox',
            'label' => $this->translate('Add a menu to manage token user list.'),
            'default' => false,
            'help' => $help,
            'current' => $state,
        );
        if ($state) {
            $pluginSettings['ShowSettingMenu'] = array(
                'type' => 'checkbox',
                'label' => $this->translate('Add a link in tool menu for settings.'),
                'default' => false,
                'current' => $this->get('ShowSettingMenu'),
            );
        }
        return $pluginSettings;
    }
    /** @inheritdoc **/
    public function saveSettings($settings)
    {
        if (!Permission::model()->hasGlobalPermission('settings', 'update')) {
            throw new CHttpException(403);
        }
        parent::saveSettings($settings);
        if (version_compare(App()->getConfig("versionnumber"), "3", "<")) {
            return;
        }
        $this->rarManageNewMenu();
    }

    /**
     * Action for create or delete a menu
     * @param $string menu name
     * @return void
     */
    private function rarManageNewMenu()
    {
        $oSurveymenuEntries = SurveymenuEntries::model()->find("name = :name", array(":name" => 'ReloadAnyResponseSettings'));
        $createSettingMenu = App()->getRequest()->getPost('createReloadAnyResponseSettings');
        if (empty($oSurveymenuEntries) && $createSettingMenu) {
            $parentMenu = 1;
            $order = 3;
            /* Find token menu */
            $oTokensMenuEntries = SurveymenuEntries::model()->find("name = :name", array(":name" => 'tokens'));
            if ($oTokensMenuEntries) {
                $parentMenu = $oTokensMenuEntries->menu_id;
                $order = $oTokensMenuEntries->ordering;
                // Add it before token
            }
            /* Unable to translate it currently … */
            $aNewMenu = array(
                // staticAddMenuEntry didn't use it but parse title
                'name' => 'ReloadAnyResponseSettings',
                'language' => 'en-GB',
                'title' => "Reload any response management",
                'menu_title' => "Reload any response",
                'menu_description' => "Settings of reload any response plugin.",
                'menu_icon' => 'refresh',
                'menu_icon_type' => 'fontawesome',
                'menu_link' => 'admin/pluginhelper',
                'manualParams' => array(
                    'sa' => 'sidebody',
                    'plugin' => 'reloadAnyResponse',
                    'method' => 'actionSettings',
                ),
                'permission' => 'surveysettings',
                'permission_grade' => 'update',
                'pjaxed' => false,
                'hideOnSurveyState' => 'none',
                'addSurveyId' => true,
                'addQuestionGroupId' => false,
                'addQuestionId' => false,
                'linkExternal' => false,
                'ordering' => $order,
            );
            $iMenu = SurveymenuEntries::staticAddMenuEntry($parentMenu, $aNewMenu);
            $oSurveymenuEntries = SurveymenuEntries::model()->findByPk($iMenu);
            if ($oSurveymenuEntries) {
                $oSurveymenuEntries->ordering = $order;
                $oSurveymenuEntries->name = 'ReloadAnyResponseSettings'; // SurveymenuEntries::staticAddMenuEntry cut name, then reset
                $oSurveymenuEntries->save();
                SurveymenuEntries::reorderMenu($parentMenu);
            }
        }
        if (!empty($oSurveymenuEntries) && empty($createSettingMenu)) {
            SurveymenuEntries::model()->deleteAll("name = :name", array(":name" => 'ReloadAnyResponseSettings'));
        }
    }
    /** @inheritdoc
     * Add some specific settings in TULAM
     **/
    public function beforeTULAMSurveySettings()
    {
        $SurveySettingsEvent = $this->getEvent();
        $surveyId = $SurveySettingsEvent->get('survey');
        $oSurvey = Survey::model()->findByPk($surveyId);
        $allowTokenDefault = $this->get('allowTokenUser', null, null, $this->settings['allowTokenUser']['default']) ? gT('Yes') : gT('No');
        $allowTokenGroupUserDefault = $this->get('allowTokenGroupUser', null, null, $this->settings['allowTokenGroupUser']['default']) ? gT('Yes') : gT('No');
        $allowTokenUserHelp = $this->getTokenUserhelp($oSurvey);
        $SurveySettingsEvent->set(
            "settings.{$this->id}",
            array(
                'name' => get_class($this),
                'settings' => array(
                    'allowTokenUser' => array(
                        'type' => 'select',
                        'label' => $this->translate("Allow participant with token to reload or create responses."),
                        'help' => $allowTokenUserHelp,
                        'options' => array(
                            1 => gT("Yes"),
                            0 => gT("No"),
                        ),
                        'htmlOptions' => array(
                            'empty' => CHtml::encode(sprintf($this->translate("Use default (%s)"), $allowTokenDefault)),
                        ),
                        'current' => $this->get('allowTokenUser', 'Survey', $surveyId, "")
                    ),
                    'allowTokenGroupUser' => array(
                        'type' => 'select',
                        'label' => $this->translate("Allow participant with token in same group to reload responses."),
                        'help' => $this->translate("User must be allowed to reload own reponse. If you use another plugin, like questionExtraSurvey : you don't need to set it here."),
                        'options' => array(
                            1 => gT("Yes"),
                            0 => gT("No"),
                        ),
                        'htmlOptions' => array(
                            'empty' => CHtml::encode(sprintf($this->translate("Use default (%s)"), $allowTokenGroupUserDefault)),
                        ),
                        'current' => $this->get('allowTokenGroupUser', 'Survey', $surveyId, "")
                    ),
                    'allowUserOnSubmitted' => array(
                        'type' => 'select',
                        'label' => $this->translate("On submitted response."),
                        'help' => $this->translate("If user is allowed to edit response : allowed to edit submitted response too. This was checked even if another plugin allow edition."),
                        'options' => array(
                            0 => gT("No"),
                        ),
                        'htmlOptions' => array(
                            'empty' => CHtml::encode($this->translate("Not restricted")),
                        ),
                        'current' => $this->get('allowUserOnSubmitted', 'Survey', $surveyId, "")
                    ),
                    'allowTokenGroupManager' => array(
                        'type' => 'select',
                        'label' => $this->translate("Allow manager of group to reload response"),
                        'help' => $this->translate("Checked only if user on same group didn't have this right."),
                        'options' => array(
                            1 => gT("Yes"),
                        ),
                        'htmlOptions' => array(
                            'empty' => $this->translate("Follow user right."),
                        ),
                        'current' => $this->get('allowTokenGroupManager', 'Survey', $surveyId, "")
                    ),
                    'resetTokenWhenGroupManager' => array(
                        'type' => 'select',
                        'label' => $this->translate("When manager of group reload response update original token"),
                        'help' => $this->translate("If all user of group can not reload response, this allow manager to reload response transparently. Response is reload with token of manager but resetted at end."),
                        'options' => array(
                            1 => gT("Yes"),
                        ),
                        'htmlOptions' => array(
                            'empty' => $this->translate("No, set token to manager."),
                        ),
                        'current' => $this->get('resetTokenWhenGroupManager', 'Survey', $surveyId, "")
                    ),
                    'allowTokenGroupManagerOnSubmitted' => array(
                        'type' => 'select',
                        'label' => $this->translate("Allow manager of group to reload submitted response"),
                        'help' => $this->translate("Checked only if user on same group didn't have this right."),
                        'options' => array(
                            1 => gT("Yes"),
                        ),
                        'htmlOptions' => array(
                            'empty' => $this->translate("Follow user right."),
                        ),
                        'current' => $this->get('allowTokenGroupManagerOnSubmitted', 'Survey', $surveyId, "")
                    ),
                )
            )
        );
    }

    /**
     * See event
     * Save the value set in settings
     * return void
     */
    public function newTULAMSurveySettings()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $SurveySettingsEvent = $this->getEvent();
        $surveyId = $SurveySettingsEvent->get('survey');
        $settings = $SurveySettingsEvent->get('settings');
        foreach ($settings as $setting => $value) {
            $this->set($setting, $value, 'Survey', $surveyId);
        }
    }

    /**
     * When need to do something :
     * - fix survey setting before beforeSurveyPage
     * - add script on browse response
     * - replace editdata link
     * @return void
     */
    public function beforeControllerAction()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        if ($this->getEvent()->get("controller") == "survey" && $this->getEvent()->get("action") == "index") {
            $this->fixSurveyAttributes();
            return;
        }
        if ($this->getEvent()->get("controller") == "admin") {
            if ($this->getEvent()->get("action") != "responses" && $this->getEvent()->get("action") != "dataentry") {
                return;
            }
        } elseif ($this->getEvent()->get("controller") != "responses") {
            return;
        }
        $isResponseList = ($this->getEvent()->get("action") == "responses" || $this->getEvent()->get("controller") == "responses");
        $surveyid = App()->getRequest()->getParam('surveyid', App()->getRequest()->getParam('surveyId'));
        if (!Permission::model()->hasSurveyPermission($surveyid, 'response', 'update')) {
            return;
        }
        if (!\reloadAnyResponse\Utilities::SurveyIsValid($surveyid)) {
            return;
        }
        if (
            $isResponseList &&
            $this->get('addEditResponseOnBrowse', null, null, 1) &&
            !$this->get('replaceEditResponse', null, null, 0)
        ) {
            $reloadAnyResponseOptions = array(
                'baseUrl' => App()->createUrl("survey/index", array('sid' => $surveyid, 'newtest' => 'Y', 'srid' => "")),
                'lang' => array(
                    'launch' => gT("Execute Survey"),
                ),
                'lsVersion' => floatval(App()->getConfig('versionnumber'))
            );
            App()->clientScript->registerScript(
                'reloadAnyResponseAdminScript',
                'LS.reloadAnyResponse = ' . json_encode($reloadAnyResponseOptions),
                CClientScript::POS_BEGIN
            );
            $scriptname = 'browse_response';
            if (intval(App()->getConfig('versionnumber')) < 6) {
                $scriptname = 'browse_response_pre6';
            }
            App()->clientScript->registerScriptFile(
                App()->assetManager->publish(dirname(__FILE__) . '/assets/browseResponse/' . $scriptname . '.js'),
                CClientScript::POS_END
            );
            return;
        }
        if ($this->getEvent()->get("action") != "dataentry") {
            return;
        }
        $sa = App()->getRequest()->getQuery("sa");
        $srid = App()->getRequest()->getQuery("id");
        if ($sa != 'editdata' || !$srid) {
            return;
        }
        if (!$this->get('replaceEditResponse', null, null, 0) && !App()->getRequest()->getQuery("executesurvey")) {
            return;
        }
        if (!\reloadAnyResponse\Utilities::AdminAllowEdit($surveyid, $srid, false)) {
            return;
        }
        $criteria = \reloadAnyResponse\Utilities::getResponseCriteria($surveyid, $srid);
        $oResponse = Response::model($surveyid)->find($criteria);
        if (empty($oResponse)) {
            return;
        }
        $surveyLink = App()->createUrl(
            "survey/index",
            array(
                'sid' => $surveyid,
                'srid' => $srid,
                'newtest' => 'Y',
                'lang' => App()->getRequest()->getQuery("browselang")
            )
        );
        $this->getEvent()->set('run', false);
        App()->getController()->redirect($surveyLink);
    }

    /**
     * Check if survey seetings need to be updated with current params
     * @return void
     */
    private function fixSurveyAttributes()
    {
        $sid = App()->getRequest()->getParam(
            'sid',
            App()->getRequest()->getParam('surveyid')
        );
        $oSurvey = Survey::model()->findByPk($sid);
        if (empty($oSurvey)) {
            return;
        }
        if ($oSurvey->active != 'Y') {
            return;
        }
        if ($oSurvey->alloweditaftercompletion == 'Y' && $oSurvey->tokenanswerspersistence != 'Y') {
            return;
        }
        $srid = App()->getRequest()->getParam('srid');
        if (empty($srid)) {
            $srid = \reloadAnyResponse\Utilities::getCurrentReloadedSrid($sid);
        }
        if (empty($srid)) {
            return;
        }
        if (Permission::model()->hasSurveyPermission($sid, 'responses', 'update') && \reloadAnyResponse\Utilities::getReloadAnyResponseSetting($sid, 'allowAdminUser')) {
            $startUrl = new \reloadAnyResponse\StartUrl($sid);
            if ($startUrl->isAvailable()) {
                $this->enablealloweditaftercompletion = true;
                Survey::model()->resetCache();
                $this->subscribe('afterFindSurvey', 'setSurveyAttributes');
                Survey::model()->findByPk($sid);
                return;
            }
        }
        $token = App()->getRequest()->getParam('token');
        if (empty($token)) {
            $token = \reloadAnyResponse\Utilities::getCurrentReloadedToken($sid);
        }

        if (!empty($token)) {
            if ($srid == "new") {
                /* @todo : allow create system and checker */
                if (\reloadAnyResponse\Utilities::getReloadAnyResponseSetting($sid, 'allowTokenUser')) {
                    $this->disabletokenanswerspersistence = true;
                    $this->enablealloweditaftercompletion = true;
                    Survey::model()->resetCache();
                    $this->subscribe('afterFindSurvey', 'setSurveyAttributes');
                    Survey::model()->findByPk($sid);
                }
                return;
            }
            $allowEdit = \reloadAnyResponse\Utilities::TokenAllowEdit($sid, $srid, $token);
            if (!$allowEdit) {
                return;
            }
            $this->enablealloweditaftercompletion = true;
            Survey::model()->resetCache();
            $this->subscribe('afterFindSurvey', 'setSurveyAttributes');
            Survey::model()->findByPk($sid);
            return;
        }
        if (!App()->getRequest()->getParam('code')) {
            return;
        }
        /* Here with accesscode only : review system */
        $startUrl = new \reloadAnyResponse\StartUrl($sid);
        if (!$startUrl->isAvailable()) {
            return;
        }
        $this->enablealloweditaftercompletion = true;
        Survey::model()->resetCache();
        $this->subscribe('afterFindSurvey', 'setSurveyAttributes');
        Survey::model()->findByPk($sid);
    }

    /**
     * Set a surey as editable
     * @see afterFindSurvey event
     */
    public function setSurveyAttributes()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        if ($this->enablealloweditaftercompletion) {
            $this->getEvent()->set('alloweditaftercompletion', 'Y');
        }
        if ($this->disabletokenanswerspersistence) {
            $this->getEvent()->set('tokenanswerspersistence', 'N');
        }
    }

    /** @inheritdoc
     * Delete all response link when survey is set to active != Y
     **/
    public function afterSurveySave()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        if ($this->getCurrentSetting('deleteLinkWhenSurveyDeactivated')) {
            $oSurvey = $this->getEvent()->get('model');
            if ($oSurvey->sid && $oSurvey->active != 'Y') {
                $deleted = \reloadAnyResponse\models\responseLink::model()->deleteAll("sid = :sid", array(':sid' => $oSurvey->sid));
                if ($deleted > 0) { // Don't log each time, can be saved for something other …
                    $this->log(sprintf("%d responseLink deleted for %d", $deleted, $oSurvey->sid), CLogger::LEVEL_INFO);
                }
            }
        }
    }

    /** @inheritdoc
     * Delete all response link when survey is deleted
     **/
    public function afterSurveyDelete()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        if ($this->getCurrentSetting('deleteLinkWhenSurveyDeleted')) {
            $oSurvey = $this->getEvent()->get('model');
            if ($oSurvey->sid) {
                $deleted = \reloadAnyResponse\models\responseLink::model()->deleteAll("sid = :sid", array(':sid' => $oSurvey->sid));
                if ($deleted > 0) { // Don't log each time, can be saved for something other …
                    $this->log(sprintf("%d responseLink deleted for %d", $deleted, $oSurvey->sid), CLogger::LEVEL_INFO);
                }
            }
        }
    }

    /** @inheritdoc
     * Delete all response link when surveys is deleted
     * @todo
     **/
    public function beforeSurveyDeleteMany()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        if ($this->getCurrentSetting('deleteLinkWhenSurveyDeleted')) {
            $criteria = $this->getEvent()->get('filterCriteria');
        }
    }

    /** @inheritdoc
     * Create the response link when survey is started
     * Remind : it's better if your plugin create this link directly `$responseLink = \reloadAnyResponse\models\responseLink::setResponseLink($iSurvey,$iResponse,$token);`
     * Before 3.15.0 : afterResponseSave event didn't exist
     **/
    public function afterModelSave()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $oModel = $this->getEvent()->get('model');
        $className = get_class($oModel);
        /* Create responlink for survey and srid (work when start a survey) */
        if ($className == 'SurveyDynamic' || $className == 'Response') {
            $sid = str_replace(array('{{survey_', '}}'), array('', ''), $oModel->tableName());
            /* Test for sid activation */
            if (!$this->getIsActivated('uniqueCodeCreate', $sid)) {
                return;
            }
            $srid = isset($oModel->id) ? $oModel->id : null;
            if ($sid && $srid) {
                $responseLink = \reloadAnyResponse\models\responseLink::model()->findByPk(array('sid' => $sid, 'srid' => $srid));
                /* @todo : add a way to reset potential token ?
                 * @see https://gitlab.com/SondagesPro/managament/responseListAndManage/blob/80fb8571d394eedda6abfbfd1757c5322f699608/responseListAndManage.php#L2336
                 **/
                if (!$responseLink) {
                    $token = isset($oModel->token) ? $oModel->token : null;
                    $responseLink = \reloadAnyResponse\models\responseLink::setResponseLink($sid, $srid, $token);
                    if (!$responseLink) {
                        $this->log("Unable to save responseLink with following errors.", CLogger::LEVEL_ERROR);
                        $this->log(CVarDumper::dumpAsString($responseLink->getErrors()), CLogger::LEVEL_ERROR);
                    }
                }
            }
        }
    }

    /**
     * @inheritdoc
     * Delete related responseLink when a response is deleted
     * Before 3.15.0 : afterResponseSave event didn't exist
     * This function is in testing currently
     **/
    public function afterModelDelete()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        if ($this->getCurrentSetting('deleteLinkWhenResponseDeleted')) {
            $oModel = $this->getEvent()->get('model');
            $className = get_class($oModel);
            if ($className == 'SurveyDynamic' || $className == 'Response') {
                $sid = str_replace(array('{{survey_', '}}'), array('', ''), $oModel->tableName());
                $srid = isset($oModel->id) ? $oModel->id : null;
                if ($srid) {
                    \reloadAnyResponse\models\responseLink::model()->deleteByPk(array('sid' => $sid, 'srid' => $srid));
                }
            }
        }
    }

    /** @See event */
    public function beforeLoadResponse()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $srid = App()->getRequest()->getQuery('srid');
        $surveyId = $this->getEvent()->get('surveyId');
        $oSurvey = Survey::model()->findByPk($surveyId);
        $oResponses = $this->getEvent()->get('responses');
        $oResponse = $this->getEvent()->get('response'); /* Get the current response set */
        $token = App()->getRequest()->getParam('token');

        /* control multi access to token with token and allow edit reponse */
        $oSurvey = Survey::model()->findByPk($surveyId);
        if (empty($oSurvey) || $oSurvey->tokenanswerspersistence != "Y") {
            return;
        }
        if ($srid == 'new' && !$this->get('tokenLoadAllowForceNew', 'Survey', $surveyId, 1)) {
            $srid = null;
        }
        if ($srid == 'new' && $token && $this->getIsActivated('allowTokenUser', $surveyId)) {
            \reloadAnyResponse\Utilities::setCurrentReloadedWay($surveyId, 'token');
        }
        if ($srid == 'new' && $token && $this->getIsActivated('allowTokenUser', $surveyId)) {
            if ($oSurvey->tokenanswerspersistence != "Y" || $oSurvey->showwelcome != 'Y' || $oSurvey->format == 'A') {
                /* Check if create or not … */
                $this->getEvent()->set('response', false);
                return;
            }
        }
        /* if srid is set : all is already done */
        /* need to check maxstep */
        if (!$this->getCurrentSetting('multiAccessTime', $surveyId)) {
            return;
        }
        if (is_null($oResponse)) {
            if (empty($oResponses[0]->submitdate) || $oSurvey->alloweditaftercompletion == 'Y') {
                $oResponse = $oResponses[0];
            }
        }

        if (empty($oResponse)) {
            return;
        }
        if ($since = \reloadAnyResponse\models\surveySession::getIsUsed($surveyId, $oResponse->id, !$this->noactionByUserAgent())) {
            $this->endWithEditionMessage($since);
        }
        if (!$this->noactionByUserAgent()) {
            \reloadAnyResponse\models\surveySession::saveSessionTime($surveyId, $oResponse->id);
        }
    }

    /** @inheritdoc **/
    public function beforeSurveyPage()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        /* Save current session Id to allow same user to reload survey in same browser */
        /* resetAllSessionVariables regenerate session id */
        /* Keep previous session id, if user reload start url it reset the sessionId, need to leav access */
        $surveyid = $this->getEvent()->get('surveyId');
        $oSurvey = Survey::model()->findByPk($surveyid);
        if (empty($oSurvey)) {
            return;
        }
        if (!$oSurvey->getIsActive()) {
            return;
        }
        /* Token control */
        $currentToken = \reloadAnyResponse\Utilities::getCurrentToken($surveyid);
        if (!$this->controlTokenAccess($oSurvey, $currentToken)) {
            /* @todo : better control  for admoin permission */
            if (!empty($currentToken) || !Permission::model()->hasSurveyPermission($surveyid, 'responses', 'update')) {
                /* Token exist and didn't have access (outdated, not same session etc …) : leave LimeSurvey do */
                return;
            }
        }
        /* Multiple access to same survey checking */
        $multiAccessTime = $this->getCurrentSetting('multiAccessTime', $surveyid);
        if ($multiAccessTime !== '') {
            App()->setConfig('surveysessiontime_limit', $multiAccessTime);
        }
        $disableMultiAccess = true;
        if ($multiAccessTime === '0' /* disable by survey */ || $multiAccessTime === '' /* disable globally */) {
            $disableMultiAccess = false;
        }
        if ($disableMultiAccess) {
            $this->checkAccessByUserAgent();
            $this->surveyId = $surveyid;
            $this->subscribe('beforeTwigRenderTemplate', 'addUnloadScript');
        }

        /* For token : @todo in beforeReloadReponse */
        /* @todo : delete surveySession is save or clearall action */
        if ($disableMultiAccess && ($since = \reloadAnyResponse\models\surveySession::getIsUsed($surveyid, null, !$this->noactionByUserAgent(), self::getPageId()))) {
            /* This one is done with current session : maybe allow to keep srid in session and reload it ? */
            killSurveySession($surveyid);
            $this->endWithEditionMessage($since);
        }
        /* Clear all action */
        $isClearAll = (App()->request->getPost('clearall') == 'clearall' || App()->request->getPost('move') == 'clearall') && App()->request->getPost('confirm-clearall') == 'confirm';
        if ($isClearAll) {
            $this->actionOnClearAll($surveyid);
            /* If we are there : LS must do it's own action */
            return;
        }

        $currentSrid = \reloadAnyResponse\Utilities::getCurrentSrid($surveyid);
        /* If POST : token is set to manager, need to reset in case */
        if (App()->getRequest()->isPostRequest && $currentToken && $currentSrid) {
            if (
                $this->getCurrentSetting('resetTokenWhenGroupManager', $surveyid)
                && \reloadAnyResponse\Utilities::getCurrentTokenIsManager($surveyid, $currentToken)
            ) {
                $oReponseToken = \SurveyDynamic::model($surveyid)->findByPk($currentSrid);
                if ($oReponseToken && !empty($oReponseToken->token)) {
                    $this->originalToken = $oReponseToken->token;
                }
            }
        }
        /* Check POST and current session : throw error if needed */
        if (App()->getRequest()->getPost('reloadAnyResponseSrid')) {
            $currentReloadedSrid = \reloadAnyResponse\Utilities::getCurrentReloadedSrid($surveyid);
            \reloadAnyResponse\Utilities::setSaveAutomatic($surveyid);
            if ($currentSrid != App()->getRequest()->getPost('reloadAnyResponseSrid') || ($currentReloadedSrid && $currentReloadedSrid != $currentSrid)) {
                throw new CHttpException(400, $this->translate("Your current session seems invalid with current data."));
            }
            if ($disableMultiAccess && ($since = \reloadAnyResponse\models\surveySession::getIsUsed($surveyid, $currentSrid, !$this->noactionByUserAgent(), self::getPageId()))) {
                killSurveySession($surveyid);
                $this->endWithEditionMessage($since);
            }
            $token = \reloadAnyResponse\Utilities::getCurrentReloadedToken($surveyid);
            \reloadAnyResponse\Utilities::resetLoadedReponse($surveyid, $currentSrid, $token, $this->getCurrentSetting('reloadResetSubmitted', $surveyid));
            $this->surveyId = $surveyid;
            $this->reloadedSrid = $currentSrid;
            return;
        }

        /* Check srid */
        $srid = App()->getRequest()->getQuery('srid');
        if (!is_null($srid) && !ctype_digit($srid) && $srid != "new") {
            $srid = null;
        }
        $token = App()->getRequest()->getParam('token');
        if (!$srid) {
            $this->reloadedSrid = \reloadAnyResponse\Utilities::getCurrentReloadedSrid($surveyid);
            if ($this->reloadedSrid && is_int($this->reloadedSrid) && $disableMultiAccess && ($since = \reloadAnyResponse\models\surveySession::getIsUsed($surveyid, $this->reloadedSrid, !$this->noactionByUserAgent(), self::getPageId()))) {
                killSurveySession($surveyid);
                $this->endWithEditionMessage($since);
            }
            if ($disableMultiAccess && !$this->noactionByUserAgent()) {
                /* Always save current srid if needed , only reload can disable this */
                \reloadAnyResponse\models\surveySession::saveSessionTime($surveyid);
            }
            if ($currentSrid && $this->getCurrentSetting('reloadResetSubmitted', $surveyid) == 'forced') {
                \SurveyDynamic::model($surveyid)->updateByPk($currentSrid, array('submitdate' => null));
            }
            $this->surveyId = $surveyid;
            return;
        }

        if ($srid == "new" || \reloadAnyResponse\Utilities::getCurrentReloadedSrid($surveyid) == "new") {
            $oStarurl = new \reloadAnyResponse\StartUrl($surveyid, $token);
            if ($oStarurl->isAvailable()) {
                $this->reloadedSrid = "new";
                $this->surveyId = $surveyid;
            }
            return;
        }
        if ($disableMultiAccess && ($since = \reloadAnyResponse\models\surveySession::getIsUsed($surveyid, $srid, !$this->noactionByUserAgent(), self::getPageId()))) {
            killSurveySession($surveyid);
            $this->endWithEditionMessage($since);
        }
        if ($this->loadReponse($surveyid, $srid, App()->getRequest()->getParam('token'), App()->getRequest()->getParam('code'))) {
            $this->surveyId = $surveyid;
            $this->reloadedSrid = $srid;
            $this->reloadedToken = App()->getRequest()->getParam('token');
        }
    }

    /**
     * @see beforeTwigRenderTemplate event
     * Do some action when srid is potentially set
     */
    public function beforeTwigRenderTemplate()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        if (!$this->surveyId) {
            return;
        }
        \reloadAnyResponse\Utilities::fixToken($this->surveyId);
        $srid = \reloadAnyResponse\Utilities::getCurrentSrid($this->surveyId);
        if ($this->originalToken) {
            \SurveyDynamic::model($this->surveyId)->updateByPk($srid, array('token' => $this->originalToken));
        }
        if ($this->reloadedSrid == "new") {
            if ($srid) {
                \reloadAnyResponse\Utilities::setSaveAutomatic($this->surveyId);
                $this->reloadedSrid = \reloadAnyResponse\Utilities::getCurrentSrid($this->surveyId);
                \reloadAnyResponse\Utilities::setCurrentReloadedSrid($this->surveyId, $srid);
                $this->reloadedSrid = $srid;
            } else {
                /* What happen ? Unsure on what to do . */
                \reloadAnyResponse\Utilities::setCurrentReloadedSrid($this->surveyId, null);
                $this->reloadedSrid = null;
            }
        }
        if ($srid) {
            if (\reloadAnyResponse\Utilities::getSetting($this->surveyId, 'keepMaxStep')) {
                \reloadAnyResponse\Utilities::setMaxStep($this->surveyId, $srid);
            }
        }
    }

    /**
     * @see beforeTwigRenderTemplate event
     * Add the script when unload page
     */
    public function addUnloadScript()
    {
        if (!$this->surveyId) {
            return;
        }
        $srid = \reloadAnyResponse\Utilities::getCurrentSrid($this->surveyId);
        if (!$srid) {
            return;
        }
        $ajaxUrl = App()->getController()->createUrl(
            'plugins/direct',
            array('plugin' => get_class($this), 'function' => 'close', 'sid' => $this->surveyId, 'srid' => $srid)
        );
        $onBeforeUnload = "$(window).on('unload', function() { \n";
        if (self::getPageId()) {
            $onBeforeUnload .= "    let beaconData = new FormData();\n";
            $onBeforeUnload .= "    beaconData.append('" . App()->request->csrfTokenName . "', '" . App()->request->csrfToken . "');;\n";
            $onBeforeUnload .= "    beaconData.append('pageId', '" . self::getPageId() . "');;\n";
            $onBeforeUnload .= "    navigator.sendBeacon('{$ajaxUrl}', beaconData);\n";
        } else {
            $onBeforeUnload .= "    jQuery.ajax({ async: false, url:'{$ajaxUrl}' });\n";
        }
        $onBeforeUnload .= "})\n";
        $onBeforeUnload .= "function closeReloadAnyResponse() { \n";
        $onBeforeUnload .= "    jQuery.ajax({ async: false, url:'{$ajaxUrl}' });\n";
        $onBeforeUnload .= "}\n";
        App()->getClientScript()->registerScript("reloadAnyResponseBeforeUnload", $onBeforeUnload, CClientScript::POS_END);
    }

    /**
     * @see getPluginTwigPath event
     * Here : adding path to twig (when needed only)
     */
    public function addPluginTwigPath()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $viewPath = dirname(__FILE__) . "/twig";
        $this->getEvent()->append('add', array($viewPath));
    }

    /**
     * @see beforeQuestionRender event
     * Adding a POST value with current reloaded Srid
     * @return void
     */
    public function beforeQuestionRender()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $this->unsubscribe('beforeQuestionRender');
        if (!$this->reloadedSrid) {
            return;
        }
        if (App()->request->getParam('loadall') == "reload") {
            /* Just reloaded via LimeSurvey core */
            $surveyId = $this->getEvent()->get('surveyId');
            $srid = \reloadAnyResponse\Utilities::getCurrentSrid($surveyId);
            $this->reloadedSrid = $srid;
            \reloadAnyResponse\Utilities::setCurrentReloadedSrid($surveyId, $srid);
            return;
        }
        if (is_int($this->reloadedSrid) || ctype_digit($this->reloadedSrid)) {
            $hiddenInput = CHtml::hiddenField('reloadAnyResponseSrid', $this->reloadedSrid);
            $this->getEvent()->set("answers", $this->getEvent()->get("answers") . $hiddenInput);
            $this->reloadedSrid = null;
        }
    }
    /**
     * Delete SurveySession for this event srid
     */
    public function afterSurveyDeleteSurveySession()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $surveyId = $this->getEvent()->get('surveyId');
        $responseId = $this->getEvent()->get('responseId');
        if (!empty($surveyId) && !empty($responseId)) {
            \reloadAnyResponse\models\surveySession::model()->deleteByPk(array('sid' => $surveyId, 'srid' => $responseId));
        }
    }

    /** @inheritdoc **/
    public function newDirectRequest()
    {

        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        if ($this->getEvent()->get('target') != get_class($this)) {
            return;
        }
        switch ($this->getEvent()->get('function')) {
            case 'delete':
                $this->directDelete();
                break;
            case 'close':
                $this->directSessionDelete();
                break;
            default:
                // Nothing to do
        }
    }

    /**
     * Direct delete session
     * To be used via ajax request, session deleted use current GET param
     * @throws error
     * @return void
     */
    private function directSessionDelete()
    {
        @ignore_user_abort(true);
        $surveyId = App()->getRequest()->getParam('sid');
        $responseId = App()->getRequest()->getParam('srid');
        $pageId = App()->getRequest()->getParam('pageId');
        /* By pageId */
        if ($surveyId && $responseId && $pageId) {
            $count = \reloadAnyResponse\models\surveySession::model()->deleteAll(
                "sid = :sid and srid = :srid and pageid = :pageid",
                array(':sid' => $surveyId, ':srid' => $responseId, ':pageid' => $pageId)
            );
            $this->renderJson([
                'count' => $count,
                'status' => 'pageId',
            ]);
        }
        /* Best way by sessionId */
        $sessionId = \reloadAnyResponse\models\surveySession::getSessionId();
        if ($sessionId) {
            $count = \reloadAnyResponse\models\surveySession::model()->deleteAll(
                "sid = :sid and srid = :srid and session = :session",
                array(':sid' => $surveyId, ':srid' => $responseId, ':session' => $sessionId)
            );
            $this->renderJson([
                'count' => $count,
                'status' => 'sessionId',
            ]);
        } else {
            if (\reloadAnyResponse\Utilities::getCurrentReloadedSrid($surveyId) != $responseId && \reloadAnyResponse\Utilities::getCurrentSrid($surveyId) != $responseId) {
                $this->log(sprintf("Try to reset session for %s in %s", $responseId, $surveyId), \CLogger::LEVEL_INFO);
                $this->renderJson([
                    'count' => 0,
                    'status' => 'invalid',
                ]);
            } elseif ($surveyId && $responseId) {
                $count = \reloadAnyResponse\models\surveySession::model()->deleteByPk(array('sid' => $surveyId, 'srid' => $responseId));
                $this->renderJson([
                    'count' => $count,
                    'status' => 'current',
                ]);
            }
        }
        $this->renderJson([
            'count' => 0,
            'status' => 'none',
        ]);
    }

    /**
     * Direct delete after control deletion is available with current permission
     * Render json
     * @return void
     */
    private function directDelete()
    {
        if (!App()->getRequest()->isPostRequest) {
            $this->ThrowHttpException("400", gT("Bad request"));
        }
        $surveyId = App()->getRequest()->getParam('sid');
        $responseId = App()->getRequest()->getParam('srid');
        $token = App()->getRequest()->getParam('token');
        $accescode = App()->getRequest()->getParam('code');
        if (!$this->get('allowDirectDelete', 'Survey', $surveyId, "")) {
            $this->ThrowHttpException("403");
        }
        /* Control validity of deletion */
        $oStartUrl = new \reloadAnyResponse\StartUrl($surveyId, $token);
        $allowed = boolval($oStartUrl->getUrl($responseId, [], false, false, true, $accescode));

        if (!$allowed) {
            $this->renderJson([
                'success' => false,
                'message' => $this->translate("Invalid response")
            ]);
        }
        if (version_compare(App()->getConfig('RelatedSurveyManagementApiVersion', "0.0"), "0.9", ">=")) {
            $actionDeletedManaged = $this->get('useDeletedManaged', 'Survey', $surveyId, '');
            if ($actionDeletedManaged === '') {
                $actionDeletedManaged = $this->get('useDeletedManaged', null, null, $this->settings['useDeletedManaged']['default']);
            }
            if ($actionDeletedManaged) {
                $criteria = new CDbCriteria();
                $criteria->compare('id', $responseId);
                $RelatedSurveysHelper = \RelatedSurveyManagement\RelatedSurveysHelper::getInstance($surveyId);
                $RelatedSurveysHelper->deleteResponse($criteria);
                $this->renderJson([
                    'success' => true,
                    'message' => $this->translate("Response deleted")
                ]);
            }
        }
        \Response::model($surveyId)->deleteByPk($responseId);
        $this->renderJson([
            'success' => true,
            'message' => $this->translate("Response deleted")
        ]);
    }

    /**
     * action on clear all, result are end or return.
     * @param integer $surveyId
     * @return void
     */
    private function actionOnClearAll($surveyId)
    {
        $srid = \reloadAnyResponse\Utilities::getCurrentSrid($surveyId);
        if (empty($srid)) {
            return;
        }
        \reloadAnyResponse\models\surveySession::model()->deleteByPk(array('sid' => $surveyId, 'srid' => $srid));

        $reloadedSrid = App()->getRequest()->getPost('reloadAnyResponseSrid');
        if ($srid && $reloadedSrid && $reloadedSrid != $srid) {
            /* Must throw error : else potential deletion of bad survey */
            /* Lets do the default action */
            return;
        }
        if (empty($reloadedSrid)) {
            $reloadedSrid = \reloadAnyResponse\Utilities::getCurrentReloadedSrid($surveyId);
            if (empty($reloadedSrid)) {
                if (!$this->getCurrentSetting('clearAllActionForced', $surveyId)) {
                    /* seems not reloaded : quit */
                    return;
                }
            }
        }

        $this->reloadedSrid = $reloadedSrid;
        $this->currentSrid = $srid;
        $this->reloadedToken = \reloadAnyResponse\Utilities::getCurrentReloadedToken($surveyId);

        /* Always disable LS core system */
        $_POST['confirm-clearall'] = null;

        $this->surveyId = $surveyId;
        $this->subscribe('beforeTwigRenderTemplate', 'clearAllAction');
    }


    /**
     * Do the clear all action in twig event
     * Thei function is registred in beforeSurveypage
     */
    public function clearAllAction()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $this->unsubscribe('beforeTwigRenderTemplate');
        $_POST['clearall'] = null;
        $surveyId = $this->surveyId;
        $action = $this->getCurrentSetting('clearAllAction', $surveyId);
        /* Start by reset the session */
        $currentSrid = $this->currentSrid;
        $currentToken = $this->reloadedToken;
        /* what restart url to be used ? */
        /* mimic core, but replace according information */
        $restartUrlParam = array(
            'newtest' => 'Y',
            'lang' => App()->getLanguage(),
        );
        if ($currentToken) {
            $restartUrlParam['token'] = $currentToken;
        }
        $restartComplete = false;
        switch ($action) {
            case 'reset':
                $restartComplete = true;
                break;
            case 'partial':
            default:
                if (SurveyDynamic::model($surveyId)->isCompleted($currentSrid)) {
                    $restartComplete = true;
                    break;
                }
            // no break
            case 'all':
                $oResponse = Response::model($surveyId)->findByPk($currentSrid);
                if (!empty($oResponse)) { /* Can be already deleted by core */
                    if (version_compare(App()->getConfig('RelatedSurveyManagementApiVersion', "0.0"), "0.9", ">=") && $this->getCurrentSetting('clearAllActionManaged', $surveyId)) {
                        $oCriteria = new CDbCriteria();
                        $oCriteria->compare('id', $currentSrid);
                        $RelatedSurveysHelper = \RelatedSurveyManagement\RelatedSurveysHelper::getInstance($surveyId);
                        $RelatedSurveysHelper->deleteResponse($oCriteria);
                    } else {
                        if ($oResponse->delete(true)) {
                            if (Survey::model()->findByPk($surveyId)->savetimings == "Y") {
                                SurveyTimingDynamic::model($surveyId)->deleteAll("id=:srid", array(":srid" => $currentSrid)); /* delete timings ( @todo must move it to Response )*/
                            }
                            SavedControl::model()->deleteAll("sid=:sid and srid=:srid", array(":sid" => $surveyId, ":srid" => $currentSrid));
                        }
                    }
                }
        }
        killSurveySession($surveyId);
        if ($restartComplete && $this->getCurrentSetting('clearAllActionReloadSrid', $surveyId)) {
            $StartUrl = new \reloadAnyResponse\StartUrl($surveyId, $currentToken);
            $restartUrl = $StartUrl->getUrl($currentSrid, $restartUrlParam, false);
        }
        if (empty($restartUrl)) {
            $restartUrlParam = ['sid' => $surveyId] + $restartUrlParam;
            $restartUrl = App()->createUrl("survey/index", $restartUrlParam);
        }
        $aSurveyinfo = getSurveyInfo($surveyId, App()->getLanguage());
        $aSurveyinfo['surveyUrl'] = $restartUrl;
        $aSurveyinfo['newUrl'] = $restartComplete ? null : $restartUrl;
        $aSurveyinfo['restartUrl'] = $restartComplete ? $restartUrl : null;
        $aSurveyinfo['include_content'] = 'deleted';
        if ($restartComplete) {
            $aSurveyinfo['include_content'] = 'clearall';
        }
        $this->subscribe('getPluginTwigPath', 'addPluginTwigPath');
        App()->twigRenderer->renderTemplateFromFile(
            "layout_global.twig",
            array(
                'oSurvey' => Survey::model()->findByPk($surveyId),
                'aSurveyInfo' => $aSurveyinfo
            ),
            false
        );
    }

    /**
     * Get boolean value for setting activation
     * @param string existing $setting
     * @param integer $surveyid
     * @return boolean
     */
    private function getIsActivated($setting, $surveyid)
    {
        return boolval(\reloadAnyResponse\Utilities::getSetting($surveyid, $setting));
    }

    /**
     * Control token access
     * @param \Survey $surveyId
     * @param string $currentToken
     * @return boolean false : token is not set or invalid
     */
    private function controlTokenAccess($oSurvey, $currentToken)
    {
        if ($oSurvey->getHasTokensTable()) {
            $surveyId = $oSurvey->sid;
            if (empty($currentToken)) {
                return false;
            }
            if ($oSurvey->getIsAllowEditAfterCompletion()) {
                $countToken = Token::model($surveyId)->editable()->countByAttributes(array('token' => $currentToken));
            } else {
                $countToken = Token::model($surveyId)->usable()->incomplete()->countByAttributes(array('token' => $currentToken));
            }
            if ($countToken < 1) {
                return false;
            }
        }
        return true;
    }

    /**
     * Create needed DB
     * @return void
     */
    private function createDb()
    {
        if (intval($this->get("dbVersion")) >= self::$dbVersion) {
            return;
        }

        /* dbVersion not needed */
        if (!$this->api->tableExists($this, 'responseLink') && !$this->api->tableExists($this, 'surveySession')) {
            $this->api->createTable(
                $this,
                'responseLink',
                array(
                    'sid' => 'int not NULL',
                    'srid' => 'int not NULL',
                    'token' => 'text',
                    'accesscode' => 'text',
                )
            );
            $this->api->createTable(
                $this,
                'surveySession',
                array(
                    'sid' => 'int not NULL',
                    'srid' => 'int not NULL',
                    'token' => 'string(55)',
                    'session' => 'text',
                    'lastaction' => 'datetime',
                )
            );
            $this->set("dbVersion", 2);
        }

        if (!$this->get("dbVersion") || $this->get("dbVersion") < 2) {
            $tableName = $this->api->getTable($this, 'surveySession')->tableName();
            App()->getDb()->createCommand()->alterColumn($tableName, 'sid', 'int not NULL');
            App()->getDb()->createCommand()->alterColumn($tableName, 'srid', 'int not NULL');
            $tableSchema = App()->getDb()->getSchema()->getTable($tableName, true);
            $tableName = $this->api->getTable($this, 'responseLink')->tableName();
            App()->getDb()->createCommand()->alterColumn($tableName, 'sid', 'int not NULL');
            App()->getDb()->createCommand()->alterColumn($tableName, 'srid', 'int not NULL');
            $tableSchema = App()->getDb()->getSchema()->getTable($tableName, true);
            $this->set("dbVersion", 3);
        }

        if ($this->get("dbVersion") < 3) {
            $tableName = $this->api->getTable($this, 'surveySession')->tableName();
            if (empty(App()->getDb()->getSchema()->getTable($tableName)->primaryKey)) {
                App()->getDb()->createCommand()->addPrimaryKey('surveysession_sidsrid', $tableName, 'sid,srid');
                App()->getDb()->getSchema()->getTable($tableName, true);
            }
            $tableName = $this->api->getTable($this, 'responseLink')->tableName();
            if (empty(App()->getDb()->getSchema()->getTable($tableName)->primaryKey)) {
                App()->getDb()->createCommand()->addPrimaryKey('responselink_sidsrid', $tableName, 'sid,srid');
                App()->getDb()->getSchema()->getTable($tableName, true);
            }
            $this->set("dbVersion", 3);
        }
        if ($this->get("dbVersion") < 5) {
            if (!$this->api->tableExists($this, 'responseMaxstep')) {
                $this->api->createTable(
                    $this,
                    'responseMaxstep',
                    array(
                        'sid' => 'int not NULL',
                        'srid' => 'int not NULL',
                        'maxstep' => 'int not NULL',
                    )
                );
                $tableName = $this->api->getTable($this, 'responseMaxstep')->tableName();
                App()->getDb()->createCommand()->addPrimaryKey('responsemaxstep_sidsrid', $tableName, 'sid,srid');
                App()->getDb()->getSchema()->getTable($tableName, true);
                $this->set("dbVersion", 5);
            }
        }
        if ($this->get("dbVersion") < 6) {
            $tableName = $this->api->getTable($this, 'surveySession')->tableName();
            App()->getDb()->createCommand()->addColumn($tableName, 'pageid', 'string(100)');
            App()->getDb()->createCommand()->createIndex('surveysession_sidsridpageid', $tableName, 'sid,srid,pageid');
            App()->getDb()->getSchema()->getTable($tableName, true);
            $this->set("dbVersion", 6);
        }
        /* all done */
        $this->set("dbVersion", self::$dbVersion);
    }

    /**
     * Add this translation just after loaded all plugins
     * @see event afterPluginLoad
     */
    public function setConfigInAfterPluginLoad()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $this->createDb();
        if (!empty(Yii::app()->getConfig('surveysessiontime_limit'))) {
            /* Allow to force surveysessiontime_limit in config.php , to do : show it to admin */
            App()->setConfig('surveysessiontimeDisable', true);
        }
        if (empty(Yii::app()->getConfig('surveysessiontime_limit'))) {
            App()->setConfig('surveysessiontime_limit', $this->get('multiAccessTime', null, null, $this->settings['multiAccessTime']['default']));
        }
        App()->setConfig('reloadAnyResponseApi', \reloadAnyResponse\Utilities::API);
    }

    /**
     * Create Survey and add current response in $_SESSION
     * @param integer $surveyid
     * @param integer $srid
     * @param string $token
     * @param string $accesscode
     * @throws Error
     * @return void|true
     */
    private function loadReponse($surveyid, $srid, $token = null, $accesscode = null)
    {
        /* Utilities check access */
        return \reloadAnyResponse\Utilities::loadReponse($surveyid, $srid, $token, $accesscode);
    }

    /**
     * Create a new response for token
     * @todo : validate if we need it or if it's a bug in LS version tested
     * @param int $surveyid
     * @param string $token
     * @return null|\Response
     */
    private function createNewResponse($surveyid, $token)
    {
        $oSurvey = Survey::model()->findByPk($surveyid);
        if ($this->accessibleWithToken($oSurvey)) {
            return;
        }
        if ($oSurvey->tokenanswerspersistence != "Y") {
            return;
        }
        //~ if($oSurvey->alloweditaftercompletion != "Y") {
        //~ return;
        //~ }
        /* some control */
        if (
            !(
                ($this->getIsActivated('allowAdminUser', $surveyid) && Permission::model()->hasSurveyPermission($surveyid, 'response', 'create'))
                ||
                ($this->getIsActivated('allowTokenUser', $surveyid))
            )
        ) {
            // Disable here
            $this->log("Try to create a new reponse with token but without valid rights", 'warning');
            return;
        }
        $oToken = Token::model($surveyid)->findByAttributes(array('token' => $token));
        if (empty($oToken)) {
            return;
        }
        $oResponse = Response::create($surveyid);
        $oResponse->token = $oToken->token;
        $oResponse->startlanguage = App()->getLanguage();
        /* @todo generate if not set */
        if (version_compare(Yii::app()->getConfig('versionnumber'), "3", ">=")) {
            $oResponse->seed = isset($_SESSION['survey_' . $surveyid]['startingValues']['seed']) ? $_SESSION['survey_' . $surveyid]['startingValues']['seed'] : null;
        }
        $oResponse->lastpage = -1;
        if ($oSurvey->datestamp == 'Y') {
            $date = dateShift(date("Y-m-d H:i:s"), "Y-m-d H:i:s", App()->getConfig('timeadjust'));
            $oResponse->datestamp = $date;
            $oResponse->startdate = $date;
        }
        $oResponse->save();
        return $oResponse;
    }

    /**
     * see beforeToolsMenuRender event
     *
     * @return void
     */
    public function beforeToolsMenuRender()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $event = $this->getEvent();
        $surveyId = $event->get('surveyId');
        $oSurvey = Survey::model()->findByPk($surveyId);
        $iSurveyMenuEntries = SurveymenuEntries::model()->count("name = :name", array(":name" => 'ReloadAnyResponseSettings'));
        if ($iSurveyMenuEntries && !$this->get('ShowSettingMenu')) {
            return;
        }
        if (Permission::model()->hasSurveyPermission($surveyId, 'surveysettings', 'update')) {
            $aMenuItem = array(
                'label' => $this->translate('Reload response settings'),
                'iconClass' => 'fa fa-refresh',
                'href' => App()->createUrl(
                    'admin/pluginhelper',
                    array(
                        'sa' => 'sidebody',
                        'plugin' => get_class($this),
                        'method' => 'actionSettings',
                        'surveyId' => $surveyId
                    )
                ),
            );
            $menuItem = new \LimeSurvey\Menu\MenuItem($aMenuItem);
            $event->append('menuItems', array($menuItem));
        }
    }

    /**
     * Main function to replace surveySetting
     * @param int $surveyId Survey id
     *
     * @return string
     */
    public function actionSettings($surveyId)
    {
        $oSurvey = Survey::model()->findByPk($surveyId);
        if (!$oSurvey) {
            throw new CHttpException(404, gT("This survey does not seem to exist."));
        }
        if (!Permission::model()->hasSurveyPermission($surveyId, 'surveysettings', 'update')) {
            throw new CHttpException(403);
        }
        if (App()->getRequest()->getPost('save' . get_class($this))) {
            $settings = array(
                'allowAdminUser',
                'allowTokenUser',
                'allowTokenGroupUser',
                'allowDirectDelete',
                'uniqueCodeCreate',
                'uniqueCodeAccess',
                'uniqueCodeAccess',
                'extraFilters',
                'useDeletedManaged',
                'throwErrorRight',
                'extraRestrictionAdmin',
                'extraRestrictionToken',
                'extraRestrictionCode',
                'clearAllAction',
                'clearAllActionForced',
                'clearAllActionManaged',
                'clearAllActionReloadSrid',
                'multiAccessTime',
                'replaceDefaultSave',
                'reloadResetSubmitted',
                'keepMaxStep',
                'tokenLoadAllowForceNew'
            );

            foreach ($settings as $setting) {
                $this->set($setting, App()->getRequest()->getPost($setting), 'Survey', $surveyId);
            }

            $pluginSettings = App()->request->getPost('plugin', array());
            foreach ($pluginSettings as $plugin => $settings) {
                $settingsEvent = new PluginEvent('newReloadAnyResponseSurveySettings');
                $settingsEvent->set('settings', $settings);
                $settingsEvent->set('survey', $surveyId);
                $settingsEvent->set('surveyId', $surveyId);
                App()->getPluginManager()->dispatchEvent($settingsEvent, $plugin);
            }

            if (App()->getRequest()->getPost('save' . get_class($this)) == 'redirect') {
                if (version_compare(App()->getConfig('versionnumber'), '4.0.0', "<")) {
                    App()->getController()->redirect(
                        App()->getController()->createUrl(
                            'admin/survey',
                            array('sa' => 'view', 'surveyid' => $surveyId)
                        )
                    );
                }
                App()->getController()->redirect(
                    App()->getController()->createUrl(
                        'surveyAdministration/view',
                        array('surveyid' => $surveyId)
                    )
                );
            }
        }

        /* currentDefault translation */
        $useDeletedManagedDefault = $this->get('useDeletedManaged', null, null, $this->settings['useDeletedManaged']['default']) ? gT('Yes') : gT('No');
        $allowAdminUserDefault = $this->get('allowAdminUser', null, null, $this->settings['allowAdminUser']['default']) ? gT('Yes') : gT('No');
        $allowTokenDefault = $this->get('allowTokenUser', null, null, $this->settings['allowTokenUser']['default']) ? gT('Yes') : gT('No');
        $allowTokenGroupUserDefault = $this->get('allowTokenGroupUser', null, null, $this->settings['allowTokenGroupUser']['default']) ? gT('Yes') : gT('No');
        $uniqueCodeCreateDefault = $this->get('uniqueCodeCreate', null, null, $this->settings['uniqueCodeCreate']['default']) ? gT('Yes') : gT('No');
        $uniqueCodeAccessDefault = $this->get('uniqueCodeAccess', null, null, $this->settings['uniqueCodeAccess']['default']) ? gT('Yes') : gT('No');
        $throwErrorRightDefault = $this->get('throwErrorRight', null, null, $this->settings['throwErrorRight']['default']) ? gT('Yes') : gT('No');
        $replaceDefaultSaveDefault = $this->get('replaceDefaultSave', null, null, $this->settings['replaceDefaultSave']['default']);
        $clearAllActionDefault = $this->get('clearAllAction', null, null, $this->settings['clearAllAction']['default']);
        $reloadResetSubmittedDefault = $this->get('reloadResetSubmitted', null, null, $this->settings['reloadResetSubmitted']['default']) ? gT('Yes') : gT('No');
        $keepMaxStepDefault = $this->get('keepMaxStep', null, null, $this->settings['keepMaxStep']['default']) ? gT('Yes') : gT('No');

        switch ($clearAllActionDefault) {
            case 'reset':
                $clearAllActionDefault = $this->translate("Do not delete");
                break;
            case 'all':
                $clearAllActionDefault = $this->translate("Always delete");
                break;
            case 'partial':
                $clearAllActionDefault = $this->translate("Delete not submitted");
                break;
            default:
            /* Must not come here, show the current value */
        }

        switch ($replaceDefaultSaveDefault) {
            case 0:
                $replaceDefaultSaveDefault = $this->translate("never");
                break;
            case 1:
            default:
                $replaceDefaultSaveDefault = $this->translate("with access code or token");
                break;
            case 2:
                $replaceDefaultSaveDefault = $this->translate("always");
                break;
        }

        $clearAllActionForcedDefault = $this->get('clearAllActionForced', null, null, $this->settings['clearAllActionForced']['default']) ? gT('Yes') : gT('No');
        $clearAllActionManageDefault = $this->get('clearAllActionManaged', null, null, $this->settings['clearAllActionManaged']['default']) ? gT('Yes') : gT('No');
        $clearAllActionReloadSridDefault = $this->get('clearAllActionReloadSrid', null, null, $this->settings['clearAllActionReloadSrid']['default']) ? gT('Yes') : gT('No');


        $multiAccessTimeDefault = $this->get('multiAccessTime', null, null, $this->settings['multiAccessTime']['default']) ? $this->get('multiAccessTime', null, null, $this->settings['multiAccessTime']['default']) : gT('Disable');
        /* Helper */
        $allowTokenUserHelp = $this->getTokenUserhelp($oSurvey);
        $filterHtmlOptions = array();
        $aSettings[$this->translate('Permission for edition in this survey')] = array(
            /* Specific disable */
            'extraFilters' => array(
                'type' => 'text',
                'label' => $this->translate("Extra condition for allowing edition."),
                'default' => '',
                'current' => $this->get('extraFilters', 'Survey', $surveyId, ""),
                'help' => $this->translate('Filter is done when checking if response exist, then if user try  to edit an unavailable response, a 404 error is shown.')
                . "<br>" . $this->translate('One field by line, field must be a valid question code. Field and value are separated by colon (<code>:</code>) . '),
                'htmlOptions' => $filterHtmlOptions
            ),
            'useDeletedManaged' => array(
                'type' => 'select',
                'label' => $this->translate("Use deleted state from RelatedSurveyManagement."),
                'options' => array(
                    1 => gT("Yes"),
                    0 => gT("No"),
                ),
                'htmlOptions' => array(
                    'empty' => CHtml::encode(sprintf($this->translate("Use default (%s)"), $useDeletedManagedDefault)),
                ),
                'current' => $this->get('useDeletedManaged', 'Survey', $surveyId, "")
            ),
            'allowAdminUser' => array(
                'type' => 'select',
                'label' => $this->translate("Allow admin user to reload any response with response id."),
                'options' => array(
                    1 => gT("Yes"),
                    0 => gT("No"),
                ),
                'htmlOptions' => array(
                    'empty' => CHtml::encode(sprintf($this->translate("Use default (%s)"), $allowAdminUserDefault)),
                ),
                'current' => $this->get('allowAdminUser', 'Survey', $surveyId, "")
            ),
            'extraRestrictionAdmin' => array(
                'type' => 'text',
                'label' => $this->translate("Extra condition for administrator."),
                'default' => '',
                'current' => $this->get('extraRestrictionAdmin', 'Survey', $surveyId, ""),
                'help' => $this->translate('One field by line, field must be a valid question code. Field and value are separated by colon (<code>:</code>) . '),
            ),
            'allowTokenUser' => array(
                'type' => 'select',
                'label' => $this->translate("Allow participant with token to reload or create responses."),
                'help' => $allowTokenUserHelp,
                'options' => array(
                    1 => gT("Yes"),
                    0 => gT("No"),
                ),
                'htmlOptions' => array(
                    'empty' => CHtml::encode(sprintf($this->translate("Use default (%s)"), $allowTokenDefault)),
                ),
                'current' => $this->get('allowTokenUser', 'Survey', $surveyId, "")
            ),
            'allowTokenGroupUser' => array(
                'type' => 'select',
                'label' => $this->translate("Allow participant with token in same group to reload responses."),
                'help' => $this->translate("Related to group of user user by TokenUsersListAndManage or responseListAndManage plugin. User must be allowed to reload own reponse."),
                'options' => array(
                    1 => gT("Yes"),
                    0 => gT("No"),
                ),
                'htmlOptions' => array(
                    'empty' => CHtml::encode(sprintf($this->translate("Use default (%s)"), $allowTokenGroupUserDefault)),
                ),
                'current' => $this->get('allowTokenGroupUser', 'Survey', $surveyId, "")
            ),
            'extraRestrictionToken' => array(
                'type' => 'text',
                'label' => $this->translate("Extra condition for participants."),
                'default' => '',
                'current' => $this->get('extraRestrictionToken', 'Survey', $surveyId, ""),
                'help' => $this->translate('One field by line, field must be a valid question code. Field and value are separated by colon (<code>:</code>) . '),
            ),
            'uniqueCodeCreate' => array(
                'type' => 'select',
                'label' => $this->translate("Create unique code automatically."),
                'options' => array(
                    1 => gT("Yes"),
                    0 => gT("No"),
                ),
                'htmlOptions' => array(
                    'empty' => CHtml::encode(sprintf($this->translate("Use default (%s)"), $uniqueCodeCreateDefault)),
                ),
                'current' => $this->get('uniqueCodeCreate', 'Survey', $surveyId, "")
            ),
            'uniqueCodeAccess' => array(
                'type' => 'select',
                'label' => $this->translate("Allow using unique code if exist."),
                'options' => array(
                    1 => gT("Yes"),
                    0 => gT("No"),
                ),
                'htmlOptions' => array(
                    'empty' => CHtml::encode(sprintf($this->translate("Use default (%s)"), $uniqueCodeAccessDefault)),
                ),
                'current' => $this->get('uniqueCodeAccess', 'Survey', $surveyId, ""),
            ),
            'extraRestrictionCode' => array(
                'type' => 'text',
                'label' => $this->translate("Extra condition with code."),
                'default' => '',
                'current' => $this->get('extraRestrictionCode', 'Survey', $surveyId, ""),
                'help' => $this->translate('One field by line, field must be a valid question code. Field and value are separated by colon (<code>:</code>) . '),
            ),
            'allowDirectDelete' => array(
                'type' => 'select',
                'label' => $this->translate("Allow direct deletion via direct request."),
                'help' => $this->translate("With this settings other plugin can create ajax request to delete extra survey. Result of the request can be and HTML error (400, 401, 403 or 404) or json result. Action must use CRSF token."),
                'options' => array(
                    0 => gT("No"),
                    1 => $this->translate("Yes"),
                ),
                'htmlOptions' => array(
                    'empty' => CHtml::encode(sprintf($this->translate("Use default (%s)"), gT("No"))),
                ),
                'current' => $this->get('allowDirectDelete', 'Survey', $surveyId, "")
            ),
            'throwErrorRight' => array(
                'type' => 'select',
                'options' => array(
                    1 => gT("Yes"),
                    0 => gT("No"),
                ),
                'htmlOptions' => array(
                    'empty' => CHtml::encode(sprintf($this->translate("Use default (%s)"), $throwErrorRightDefault)),
                ),
                'label' => $this->translate("Throw a 401 error if try to reload without rights."),
                'help' => $this->translate("Send an http 401 error when srid is in url, but user did not have right. This allow to create new response (according to survey settings for token)"),
                'current' => $this->get('throwErrorRight', 'Survey', $surveyId, "")
            ),
        );
        /* Remove invalid settings */
        if (version_compare(App()->getConfig('RelatedSurveyManagementApiVersion', "0.0"), "0.9", "<")) {
            unset($aSettings[$this->translate('Permission for edition in this survey')]['useDeletedManaged']);
        }
        $beforeRALSurveySettings = new PluginEvent('beforeRALSurveySettings');
        $beforeRALSurveySettings->set('survey', $surveyId);
        $beforeRALSurveySettings->set('surveyId', $surveyId);
        App()->getPluginManager()->dispatchEvent($beforeRALSurveySettings);
        $aSurveyPluginsSettings = $beforeRALSurveySettings->get('settings');
        if (!empty($aSurveyPluginsSettings)) {
            $aData['aSurveyPluginsSettings'] = $aSurveyPluginsSettings;
        }
        $aSettings[$this->translate('Clear all action')] = array(
            /* Clear all action */
            'clearAllAction' => array(
                'type' => 'select',
                'options' => array(
                    'reset' => gT("Reset session, don‘t delete current response"),
                    'partial' => gT("Reset session, delete current response if was not submitted."),
                    'all' => gT("Reset session, delete current response in any condition."),
                ),
                'htmlOptions' => array(
                    'empty' => CHtml::encode(sprintf($this->translate("Use default (%s)"), $clearAllActionDefault)),
                ),
                'label' => $this->translate("Action when using clearall action."),
                'help' => $this->translate("Action to do when participant want to clear all. Default action by LimeSurvey was to delete not submitted answer."),
                'current' => $this->get('clearAllAction', 'Survey', $surveyId, "")
            ),
            'clearAllActionForced' => array(
                'type' => 'select',
                'options' => array(
                    1 => gT("Yes"),
                    0 => gT("No"),
                ),
                'htmlOptions' => array(
                    'empty' => CHtml::encode(sprintf($this->translate("Use default (%s)"), $clearAllActionForcedDefault)),
                ),
                'label' => $this->translate("Replace clearall action even if response is not reloaded."),
                'current' => $this->get('clearAllActionForced', 'Survey', $surveyId, "")
            ),
            'clearAllActionReloadSrid' => array(
                'type' => 'select',
                'options' => array(
                    1 => gT("Yes"),
                    0 => gT("No"),
                ),
                'htmlOptions' => array(
                    'empty' => CHtml::encode(sprintf($this->translate("Use default (%s)"), $clearAllActionReloadSridDefault)),
                ),
                'label' => $this->translate("Add the current response in reload link."),
                'default' => $this->translate("If user clik on reload : system reload the current reponse if it's not deleted."),
                'current' => $this->get('clearAllActionReloadSrid', 'Survey', $surveyId, "")
            ),
        );
        if (version_compare(App()->getConfig('RelatedSurveyManagementApiVersion', "0.0"), "0.9", ">=")) {
            $aSettings[$this->translate('Clear all action')]['clearAllActionManaged'] = array(
                'type' => 'select',
                'options' => array(
                    1 => gT("Yes"),
                    0 => gT("No"),
                ),
                'htmlOptions' => array(
                    'empty' => CHtml::encode(sprintf($this->translate("Use default (%s)"), $clearAllActionManageDefault)),
                ),
                'label' => $this->translate("Use managed clearall if is set."),
                'help' => $this->translate("You must choose response deletion for this case."),
                'current' => $this->get('clearAllActionManaged', 'Survey', $surveyId, "")
            );
        }
        $aSettings[$this->translate('Time limit')] = array(
            'multiAccessTime' => array(
                'type' => 'int',
                'label' => $this->translate("Time for disable multiple access (in minutes) . "),
                'help' => $this->translate("Set to 0 to disable, leave empty for default. If disable : same response can be open at same time; last saved response overwrites the previous one."),
                'htmlOptions' => array(
                    'min' => 0,
                    'placeholder' => CHtml::encode(sprintf($this->translate("Use default (%s)"), $multiAccessTimeDefault)),
                ),
                'current' => $this->get('multiAccessTime', 'Survey', $surveyId, "")
            ),
            // @todo : add auto save and quit information
            // @todo : add documentation about this function
        );
        $aSettings[$this->translate('Save and reload action')] = array(
            'replaceDefaultSave' => array(
                'type' => 'select',
                'options' => array(
                    0 => gT("Never"),
                    1 => gT("If not reloaded by admin right"),
                    2 => gT("Always (if reloaded)"),
                ),
                'htmlOptions' => array(
                    'empty' => CHtml::encode(sprintf($this->translate("Use default (%s)"), $replaceDefaultSaveDefault)),
                ),
                'label' => $this->translate("Save reloaded response transparently."),
                'help' => $this->translate("Replace the LimeSurvey save form : directly save the current reponse when user click on save all. With reloaded only : this allow admin user to create easily save action."),
                'current' => $this->get('replaceDefaultSave', 'Survey', $surveyId, "")
            ),
            'reloadResetSubmitted' => array(
                'type' => 'select',
                'options' => array(
                    'forced' => $this->translate("Always"),
                    1 => $this->translate("Yes  (if reloaded)"),
                    0 => gT("No"),
                ),
                'label' => $this->translate("Reset submitdate on the first action done."),
                'help' => $this->translate("When a submitted survey is open without action : nothing was updated, if user do an action : you can choose to reset the respnse to not submitted."),
                'htmlOptions' => array(
                    'empty' => CHtml::encode(sprintf($this->translate("Use default (%s)"), $reloadResetSubmittedDefault)),
                ),
                'current' => $this->get('reloadResetSubmitted', 'Survey', $surveyId, "")
            ),
            /* Save (and load) max step */
            'keepMaxStep' => array(
                'type' => 'select',
                'options' => array(
                    1 => gT("Yes"),
                    0 => gT("No"),
                ),
                'label' => $this->translate("Keep max page seen when load and save."),
                'help' => $this->translate("Add a solution to keep max page seen. Reload it when load response."),
                'htmlOptions' => array(
                    'empty' => CHtml::encode(sprintf($this->translate("Use default (%s)"), $keepMaxStepDefault)),
                ),
                'current' => $this->get('keepMaxStep', 'Survey', $surveyId, "")
            ),
            'tokenLoadAllowForceNew' => array(
                'type' => 'select',
                'options' => array(
                    1 => gT("Yes"),
                    0 => gT("No"),
                ),
                'label' => $this->translate("Allow to force creation of new response."),
                'help' => $this->translate("using srid param with new cazn allow to create new response on survey with toan answer persistance enable. You can disallow forcing creation of new survey."),
                'current' => $this->get('tokenLoadAllowForceNew', 'Survey', $surveyId, 1)
            ),
        );

        /* Plugin event */
        $beforeReloadAnyResponseSurveySettings = new PluginEvent('beforeReloadAnyResponseSurveySettings');
        $beforeReloadAnyResponseSurveySettings->set('survey', $surveyId);
        $beforeReloadAnyResponseSurveySettings->set('surveyId', $surveyId);
        App()->getPluginManager()->dispatchEvent($beforeReloadAnyResponseSurveySettings);
        $aSurveyPluginsSettings = $beforeReloadAnyResponseSurveySettings->get('settings');
        if (!empty($aSurveyPluginsSettings)) {
            $aData['aSurveyPluginsSettings'] = $aSurveyPluginsSettings;
        }
        $aData['pluginClass'] = get_class($this);
        $aData['surveyId'] = $surveyId;
        $aData['title'] = $this->translate("Reload any response management");
        $aData['warningString'] = null;
        $aData['aSettings'] = $aSettings;
        $aData['assetUrl'] = App()->assetManager->publish(dirname(__FILE__) . '/assets/settings');
        $content = $this->renderPartial('settings', $aData, true);
        return $content;
    }

    /**
     * Did this survey have token with reload available
     * @var \Survey
     * @return boolean
     */
    private function accessibleWithToken($oSurvey)
    {
        Yii::import('application.helpers.common_helper', true);
        return $oSurvey->anonymized != "Y" && tableExists("{{tokens_" . $oSurvey->sid . "}}");
    }

    /**
     * Ending with the delay if renderMessage is available (if not : log as error …)
     * @param float $since last edit
     * @return void
     */
    private function endWithEditionMessage($since)
    {
        $messageString = sprintf($this->translate("Sorry, someone update this response to the questionnaire a short time ago. The last action was made less than %s minutes ago."), ceil($since));
        $this->ThrowHttpException("409", $messageString);
    }

    /**
     * Check access according to HTTP_USER_AGENT
     * @throw Exception
     * @return void
     */
    private function checkAccessByUserAgent()
    {
        $noHttpUserAgent = $this->get('noHttpUserAgent', null, null, $this->settings['noHttpUserAgent']['default']);
        if (empty($_SERVER['HTTP_USER_AGENT'])) {
            if ($noHttpUserAgent == 'forbidden') {
                $this->ThrowHttpException(403, "No User Agent");
            }
            // No action to do : can return
            return;
        }
        $noBotHttpUserAgent = $this->get('botHttpUserAgent', null, null, $this->settings['botHttpUserAgent']['default']);
        if ($noBotHttpUserAgent == 'forbidden' && $this->isBotByRegexp($_SERVER['HTTP_USER_AGENT'])) {
            $this->ThrowHttpException(403, "Invalid header sent : Bot header");
        }
    }

    /**
     * Check if need action (disable access time) by user aganet
     * @return boolean
     */
    private function noactionByUserAgent()
    {
        $noHttpUserAgent = $this->get('noHttpUserAgent', null, null, $this->settings['noHttpUserAgent']['default']);
        if (empty($_SERVER['HTTP_USER_AGENT'])) {
            if ($noHttpUserAgent == '') {
                return true;
            }
            return false;
        }
        $noBotHttpUserAgent = $this->get('botHttpUserAgent', null, null, $this->settings['botHttpUserAgent']['default']);
        if ($noBotHttpUserAgent == '' && $this->isBotByRegexp($_SERVER['HTTP_USER_AGENT'])) {
            return true;
        }
        return false;
    }

    /**
     * Check access according to HTTP_USER_AGENT
     * @throw Exception
     * @return boolean
     */
    private function isBotByRegexp($useragent)
    {
        $botRegexp = $this->get('botRegexp', null, null, $this->settings['botRegexp']['default']);
        if ($botRegexp === '') {
            $botRegexp = '/bot|crawl|slurp|spider|mediapartners|lua-resty-http/i';
        }
        if (!trim($botRegexp)) {
            return false;
        }
        return preg_match($botRegexp, $useragent);
    }
    /**
     * Get current setting for current survey (use empty string as null value)
     * @param string setting to get
     * @param integer survey id
     * @return string|array|null
     */
    private function getCurrentSetting($setting, $surveyId = null)
    {
        if ($surveyId) {
            return \reloadAnyResponse\Utilities::getSetting($surveyId, $setting);
        }
        $default = (isset($this->settings[$setting]['default'])) ? $this->settings[$setting]['default'] : null;
        return $this->get($setting, null, null, $default);
    }

    /**
     * get translation
     * @param string
     * @return string
     */
    private function translate($string, $language = null)
    {
        // get by DB in case
        $translated = gT($string, 'unescaped', $language);
        /* Else from po file */
        if ($translated == $string && method_exists($this, 'gT')) {
            return $this->gT($string, 'unescaped', $language);
        }
        return $translated;
    }

    /**
     * Throw specific error
     * @param @errorCode (404/401/403 totally supported)
     * @param @errorMessage
     * @throw CHttpException
     */
    private function ThrowHttpException($errorCode, $errorMessage)
    {
        \reloadAnyResponse\Utilities::returnOrThrowException($this->surveyId, $errorCode, $errorMessage, true);
    }

    /**
     * Set language according to survey or current language
     * @param $surveyid
     * @return $void
     */
    private function fixLanguage($surveyId = null)
    {
        $currentLang = App()->getLanguage();
        if ((empty($currentLang) || $currentLang == "en_US")) {
            if (!empty($_SESSION['adminlang']) && $_SESSION['adminlang'] != "auto") {
                $currentLang = $_SESSION['adminlang'];
            } else {
                $currentLang = App()->getConfig("defaultlang");
            }
        }
        if ($surveyId) {
            $oSurvey = Survey::model()->findByPk($surveyId);
            if ($oSurvey && !in_array($currentLang, $oSurvey->getAllLanguages())) {
                $currentLang = $oSurvey->language;
            }
        }
        App()->setLanguage($currentLang);
    }

    /**
     * Get the help about token user state
     * @param Survey $oSurvey
     * @return string
     */
    public function getTokenUserhelp($oSurvey)
    {
        $allowTokenUserHelp = "<ul>";
        $allowTokenUserHelp .= "<li class='text-info'>" . $this->translate("If you use another plugin, like questionExtraSurvey : you don't need to set it here.") . "</li>";
        if ($oSurvey->getIsAnonymized()) {
            $allowTokenUserHelp .= "<li class='text-danger'>" . $this->translate("Token user can not edit response because survey is anonymous.") . "</li>";
        } else {
            $allowTokenUserHelp .= "<li class='text-success'>" . $this->translate("Token user can edit response (if plugin allow it).") . "</li>";
        }
        /* The default action */
        if (!$oSurvey->getIsAnonymized()) {
            if ($oSurvey->isAllowEditAfterCompletion && $oSurvey->isTokenAnswersPersistence) {
                $allowTokenUserHelp .= "<li class='text-info'>" . $this->translate("With token : default action is to reload last response submitted or not.") . "</li>";
            }
            if ($oSurvey->isAllowEditAfterCompletion && !$oSurvey->isTokenAnswersPersistence) {
                $allowTokenUserHelp .= "<li class='text-info'>" . $this->translate("With token : default action is to create a new response, it's the best solution to allow srid='new'.") . "</li>";
            }
        }
        $allowTokenUserHelp .= "</ul>";
        return $allowTokenUserHelp;
    }

    /**
     * Get the current unique PageUid
     * Set it of not set
     * @return string
     */
    public static function getPageId()
    {
        if (is_null(self::$pageId)) {
            self::$pageId = \App()->securityManager->generateRandomString(52);
        }
        return self::$pageId;
    }

    /**
     * @inheritdoc adding string, by default current event
     * @param string
     */
    public function log($message, $level = \CLogger::LEVEL_TRACE, $logDetail = null)
    {
        if (!$logDetail && $this->getEvent()) {
            $logDetail = $this->getEvent()->getEventName();
        } // What to put if no event ?
        \Yii::log($message, $level, 'plugin.reloadAnyResponse.' . $logDetail);
    }
    /**
     * render a json result
     * @param mixed $data
     */
    private function renderJson($data)
    {
        header('Content-type: application/json; charset=utf-8');
        echo json_encode($data);
        App()->end();
    }
}
