<?php

/**
 * get settings of reloadAnyResponse
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2022-2023 Denis Chenu <http://www.sondages.pro>
 * @license AGPL v3
 * @version 5.14.3
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

namespace reloadAnyResponse;

use App;

class Settings
{
    /* @var array DefaultSettings : the default settings if not set by DB @see reloadAnyResponse::settings */
    const DefaultSettings = array(
        'allowAdminUser' => 1,
        'allowTokenUser' => 1,
        'allowTokenGroupUser' => 1,
        'uniqueCodeCreate' => 0,
        'uniqueCodeAccess' => 1,
        'replaceDefaultSave' => 1,
        'throwErrorRight' => 0,
        'allowUserOnSubmitted' => 1,
        'allowTokenGroupManager' => 0,
        'allowTokenGroupManagerOnSubmitted' => 0,
        'extraFilters' => '',
        'useDeletedManaged' => 1,
        'extraRestrictionToken' => '',
        'extraRestrictionAdmin' => '',
        'extraRestrictionCode' => '',
        'clearAllAction' => 'partial',
        'clearAllActionForced' => 0,
        'multiAccessTime' => '',
        'replaceDefaultSave' => 1,
        'reloadResetSubmitted' => 1,
        'keepMaxStep' => 0,
    );
    /**
     * Singleton
     * @var self
     */
    private static $instance = null;

    /**
     * @var null|integer
     */
    private static $pluginid;

    /**
     * @var integer|null survey id
     */
    private $surveyId;

    /**
     * @var []
     */
    private static $surveysSettings;

    /**
     * constructor
     * @param integer survey id
     */
    public function __construct($surveyId)
    {
        $this->surveyId = $surveyId;
        if (is_null(self::$pluginid)) {
            $oPlugin = \Plugin::model()->find(
                "name = :name",
                array(":name" => 'reloadAnyResponse')
            );
            self::$pluginid = $oPlugin->id;
        }
    }

    /**
     * Get the singleton
     * @param integer survey id
     * return self
     */
    public static function getInstance($surveyId)
    {
        if ((null === self::$instance) || ($surveyId !== self::$instance->surveyId)) {
            self::$instance = new self($surveyId);
        }
        return self::$instance;
    }

    /**
     * get the setting related to the survey
     * @return string|false
     */
    public function getSetting($sSetting)
    {
        if (!isset(self::$surveysSettings[$this->surveyId])) {
            self::$surveysSettings[$this->surveyId] = [];
        }
        if (isset(self::$surveysSettings[$this->surveyId][$sSetting])) {
            return self::$surveysSettings[$this->surveyId][$sSetting];
        }
        /* This survey setting */
        $oSetting = \PluginSetting::model()->find(
            'plugin_id = :pluginid AND ' . App()->getDb()->quoteColumnName('key') . ' = :key AND model = :model AND model_id = :surveyid',
            array(
                ':pluginid' => self::$pluginid,
                ':key' => $sSetting,
                ':model' => 'Survey',
                ':surveyid' => $this->surveyId,
            )
        );

        if (!empty($oSetting)) {
            $value = json_decode($oSetting->value);
            if ($value !== '') {
                self::$surveysSettings[$this->surveyId][$sSetting] = $value;
                return $value;
            }
        }

        /* empty (string) : then get by default setting */
        $oSetting = \PluginSetting::model()->find(
            'plugin_id = :pluginid AND ' . App()->getDb()->quoteColumnName('key') . ' = :key AND model IS NULL',
            array(
                ':pluginid' => self::$pluginid,
                ':key' => $sSetting,
            )
        );

        if (!empty($oSetting)) {
            $value = json_decode($oSetting->value);
            if ($value !== '') {
                self::$surveysSettings[$this->surveyId][$sSetting] = $value;
                return $value;
            }
        }

        /* empty (string) : then get setting by default */
        if (isset(self::DefaultSettings[$sSetting])) {
            self::$surveysSettings[$this->surveyId][$sSetting] = self::DefaultSettings[$sSetting];
            return self::DefaultSettings[$sSetting];
        }
        self::$surveysSettings[$this->surveyId][$sSetting] = false;
        /* Then finally : false */
        return false;
    }
}
