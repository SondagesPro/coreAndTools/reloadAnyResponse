<!-- /reloadAnyReponse/views/settings.php @version 5.9.2-beta2 -->
<div class="row">
    <div class="col-lg-12 content-right">
      <h3 class="clearfix"><?php echo $title ?>
        <div class='pull-right hidden-xs'>
          <?php
          echo CHtml::link('<i class="fa fa-check" aria-hidden="true"></i> '.gT('Save'),"#",array('class'=>'btn btn-primary','data-click-name'=>'save'.$pluginClass,'data-click-value'=>'save'));
          echo " ";
          echo CHtml::link('<i class="fa fa-check-circle-o" aria-hidden="true"></i> '.gT('Save and close'),"#",array('class'=>'btn btn-default btn-secondary','data-click-name'=>'save'.$pluginClass,'data-click-value'=>'redirect'));
          echo " ";
          echo CHtml::link(
            gT('Close'),
            (floatval(App()->getConfig('versionnumber')) < 4) ? App()->createUrl('admin/survey',array('sa'=>'view','surveyid'=>$surveyId)) : App()->createUrl('surveyAdministration/view', array('surveyid'=>$surveyId)),
            array('class'=>'btn btn-danger')
          );
          ?>
        </div>
      </h3>
        <?php if($warningString) {
            echo CHtml::tag("p",array('class'=>'alert alert-warning'),$warningString);
        } ?>
        <?php echo CHtml::beginForm();?>
        <?php foreach($aSettings as $legend=>$settings) {
          $fieldId = CHtml::getIdByName($legend);
            $legend = CHtml::link(
              "§" ,
              "#{$fieldId}",
              array(
                'class' => 'self-link',
                'name' => "{$fieldId}",
                'aria-label' => gT("Link")
              )
            ) . " " . $legend;
          $this->widget('ext.SettingsWidget.SettingsWidget', array(
                'title'=>$legend,
                'form' => false,
                'formHtmlOptions'=>array(
                    'class'=>'form-core',
                ),
                'labelWidth'=>6,
                'controlWidth'=>6,
                'settings' => $settings,
            ));
        } ?>
        <?php if(!empty($aSurveyPluginsSettings)) {
          echo CHtml::tag('fieldset',
            array(),
            CHtml::tag('legend', array(),gT('Settings by plugins')),
            false
          );
          foreach($aSurveyPluginsSettings as $id => $plugin)  {
            $title = sprintf(gT("Settings for plugin %s"), $plugin['name']);
            $fieldId = CHtml::getIdByName($title);
            $title = CHtml::link(
              "§" ,
              "#{$fieldId}",
              array(
                'class' => 'self-link',
                'name' => "{$fieldId}",
                'aria-label' => gT("Link")
              )
            ) . " " . $title;
            $this->widget('ext.SettingsWidget.SettingsWidget', array(
                'settings' => $plugin['settings'],
                'form' => false,
                'title' => $title,
                'prefix' => "plugin[{$plugin['name']}]",
            ));
          }
        } ?>
        <div class='row'>
          <div class='col-md-6'></div>
          <div class='col-md-6 submit-buttons'>
            <?php
              echo CHtml::htmlButton('<i class="fa fa-check" aria-hidden="true"></i> '.gT('Save'),array('type'=>'submit','name'=>'save'.$pluginClass,'value'=>'save','class'=>'btn btn-primary'));
              echo " ";
              echo CHtml::htmlButton('<i class="fa fa-check-circle-o " aria-hidden="true"></i> '.gT('Save and close'),array('type'=>'submit','name'=>'save'.$pluginClass,'value'=>'redirect','class'=>'btn btn-default btn-secondary'));
              echo " ";
              echo CHtml::link(
                gT('Close'),
                (floatval(App()->getConfig('versionnumber')) < 4) ? App()->createUrl('admin/survey',array('sa'=>'view','surveyid'=>$surveyId)) : App()->createUrl('surveyAdministration/view', array('surveyid'=>$surveyId)),
                array('class'=>'btn btn-danger')
              );
            ?>
          </div>
        </div>
        <?php echo CHtml::endForm();?>
    </div>
</div>
<?php
  Yii:app()->clientScript->registerScriptFile($assetUrl.'/settings.js',CClientScript::POS_END);
?>
