<?php

/**
 * Some Utilities
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2020-2022 Denis Chenu <http://www.sondages.pro>
 * @license AGPL v3
 * @version 5.14.5
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

namespace reloadAnyResponse;

use App;
use Yii;
use CHttpException;
use Survey;
use SurveyDynamic;
use Token;

class Utilities
{
    /* @var string give information for other plugin of Plugin API version */
    const API = '5.12.0';

    /* @var array DefaultSettings : the default settings if not set by DB @see reloadAnyResponse::settings */
    const DefaultSettings = array(
        'allowAdminUser' => 1,
        'allowTokenUser' => 1,
        'allowTokenGroupUser' => 1,
        'uniqueCodeAccess' => 1,
        'uniqueCodeCreate' => 0,
        'replaceDefaultSave' => 1,
        'throwErrorRight' => 0,
        'allowUserOnSubmitted' => 1,
        'allowTokenGroupManager' => 0,
        'allowTokenGroupManagerOnSubmitted' => 0,
    );

    /* @var string[] settings that can be set in session to be forced by other plugin */
    const AllowedSessionSettings = array(
        'allowAdminUser',
        'allowTokenUser',
        'allowTokenGroupUser',
        'uniqueCodeAccess',
        'uniqueCodeCreate',
        'replaceDefaultSave'
    );

    /* @var boolean[] key are sid */
    private static $surveysIsValid = [];

    /**
     * Create Survey and add current response in $_SESSION after checked permission
     * @param integer $surveydi
     * @param integer $srid
     * @param string $token
     * @param string $accesscode
     * @param boolean $load reponse, alow to check if allowed to edit
     * @throws Errors
     * @return void|true : response is reloaded (or is available for reload);
     */
    public static function loadReponse($surveyid, $srid, $token = null, $accesscode = null, $load = true)
    {
        if (!self::SurveyIsValid($surveyid)) {
            return;
        }
        if (strval($srid) != strval(intval($srid))) {
            return self::returnOrThrowException($surveyid, 400, self::translate('Response not found.'));
        }
        if ($load && self::getCurrentSrid($surveyid) == $srid && self::getCurrentToken($surveyid, false) == $token) {
            $oStartUrl = new StartUrl($surveyid, $token);
            if ($oStartUrl->isAvailable()) {
                self::setSaveAutomatic($surveyid);
                return true;
            }
            return;
        }
        $criteria = self::getResponseCriteria($surveyid, $srid);
        $criteria->select = 'id';
        $oResponse = SurveyDynamic::model($surveyid)->find($criteria);
        if (!$oResponse) {
            return self::returnOrThrowException($surveyid, 404, self::translate('Response not found.'));
        }
        $language = App()->getLanguage();
        $oSurvey = \Survey::model()->findByPk($surveyid);
        /* Plugin event to disable access*/
        if (self::PluginEventEditDisabled($surveyid, $srid, $token, 'loadResponse')) {
            return self::returnOrThrowException($surveyid, 401, self::translate('Sorry, you don‘t have access to this response.'));
        }
        /* @var boolean, did edition is allowed with current params and settings */
        $editAllowed = false;
        /* @var string : way used for reloading */
        $wayUsed = "";
        /* we check usage by usage : plugin, accesscode , token, admin */
        if (self::PluginEventEditAllowed($surveyid, $srid, $token, 'loadResponse')) {
            $editAllowed = true;
            $wayUsed = 'plugin';
        }

        if ($accesscode && self::getReloadAnyResponseSetting($surveyid, 'uniqueCodeAccess')) {
            if (!self::AccessCodeAllowEdit($surveyid, $srid, $accesscode)) {
                return self::returnOrThrowException($surveyid, 401, self::translate('Sorry, this access code is not valid.'));
            }
            $editAllowed = true;
            $wayUsed = 'code';
        }

        if (!$editAllowed && $token) {
            /* Throw error only if not admin */
            if (self::TokenAllowEdit($surveyid, $srid, $token)) {
                $editAllowed = true;
                $wayUsed = 'token';
            }
        }

        if (!$editAllowed) {
            $havePermission = self::AdminAllowEdit($surveyid, $srid);
            if (!$havePermission) {
                return self::returnOrThrowException($surveyid, 401, self::translate('Sorry, you don‘t have access to this response.'));
            }
            $editAllowed = true;
            $wayUsed = 'admin';
        }
        /* $editAllowed is true */
        /* $oResponse is a Response */
        if ($load) {
            $reloadResponse = new reloadResponse($surveyid, $oResponse->id, $language);
            $reloadResponse->startSurvey(true, false, $token);

            /* Set session for this survey / srid */
            self::setCurrentReloadedWay($surveyid, $wayUsed);
            self::setSaveAutomatic($surveyid);
            self::setCurrentReloadedToken($surveyid, $token);
            self::setCurrentReloadedSrid($surveyid, self::getCurrentSrid($surveyid));
            models\surveySession::saveSessionTime($surveyid, $oResponse->id);
        }
        return true;
    }

    /**
     * Get the search criteria for a specific survey
     * @param integer $survey
     * @param integer|null $srid
     * @param string $extraFilterSetting, if null , get the extrafilters
     * @return criteria
     */
    public static function getResponseCriteria($surveyId, $srid = null, $extraFilters = null)
    {
        /* The filter */
        $criteria = new \CDbcriteria();

        if (version_compare(App()->getConfig('RelatedSurveyManagementApiVersion', "0.0"), "0.9" , ">=") && self::getReloadAnyResponseSetting($surveyId, 'useDeletedManaged')) {
            $RelatedSurveysHelper = \RelatedSurveyManagement\RelatedSurveysHelper::getInstance($surveyId);
            $criteria = $RelatedSurveysHelper->addFilterDeletedCriteria($criteria);
        }
        if (Yii::getPathOfAlias('getQuestionInformation')) {
            if (is_null($extraFilters)) {
                $extraFilters = trim(self::getReloadAnyResponseSetting($surveyId, 'extraFilters'));
            }
            if (!empty($extraFilters)) {
                $aColumnToCode = \getQuestionInformation\helpers\surveyCodeHelper::getAllQuestions($surveyId);
                $aCodeToColumn = array_flip($aColumnToCode);
                $availableColumns = SurveyDynamic::model($surveyId)->getAttributes();
                $aFieldsLines = preg_split('/\r\n|\r|\n/', $extraFilters, -1, PREG_SPLIT_NO_EMPTY);
                $aFiltersFields = array();
                foreach ($aFieldsLines as $aFieldLine) {
                    if (!strpos($aFieldLine, ":")) {
                        continue; // Invalid line
                    }
                    $key = substr($aFieldLine, 0, strpos($aFieldLine, ":"));
                    $value = substr($aFieldLine, strpos($aFieldLine, ":") + 1);
                    if (array_key_exists($key, $availableColumns)) {
                        $aFiltersFields[$key] = $value;
                    } elseif (isset($aCodeToColumn[$key])) {
                        $aFiltersFields[$aCodeToColumn[$key]] = $value;
                    }
                }
                foreach ($aFiltersFields as $column => $value) {
                    $criteria->compare(App()->getDb()->quoteColumnName($column), $value);
                }
            }
        }
        if (!is_null($srid)) {
            $criteria->compare("id", $srid);
        }
        return $criteria;
    }

    /**
     * get current srid for a survey
     * @param $surveyid integer
     * @return integer|null
     */
    public static function getCurrentSrid($surveyid)
    {
        if (empty($_SESSION['survey_' . $surveyid]['srid'])) {
            return null;
        }
        return $_SESSION['survey_' . $surveyid]['srid'];
    }

    /**
     * get current srid for a survey
     * @param $surveyid integer
     * @return integer|string|null
     */
    public static function getCurrentReloadedSrid($surveyid)
    {
        if (empty($_SESSION['survey_' . $surveyid]['reloadAnyResponseSrid'])) {
            return null;
        }
        return $_SESSION['survey_' . $surveyid]['reloadAnyResponseSrid'];
    }

    /**
     * get current way of current reloaded
     * @param $surveyid integer
     * @return string|null
     */
    public static function getCurrentReloadedWay($surveyid)
    {
        if (empty($_SESSION['survey_' . $surveyid]['reloadAnyResponseWay'])) {
            return null;
        }
        return $_SESSION['survey_' . $surveyid]['reloadAnyResponseWay'];
    }

    /**
     * get current reloaded token for a survey
     * @param $surveyid integer
     * @return string|null
     */
    public static function getCurrentReloadedToken($surveyid)
    {
        if (empty($_SESSION['survey_' . $surveyid]['reloadAnyResponseToken'])) {
            return null;
        }
        return $_SESSION['survey_' . $surveyid]['reloadAnyResponseToken'];
    }

    /**
     * Get if current reloaded token is manager
     * @param $surveyid integer
     * @param $token string
     * @return boolean
     */
    public static function getCurrentTokenIsManager($surveyid, $token)
    {
        if (version_compare(App()->getConfig('TokenUsersListAndManageAPI'), '0.14', "<")) {
            return false;
        }
        if (isset($_SESSION['survey_' . $surveyid]['reloadAnyResponseManagerToken']) && \reloadAnyResponse\Utilities::getCurrentToken($surveyid) == $token) {
            return $_SESSION['survey_' . $surveyid]['reloadAnyResponseManagerToken'];
        }
        $tokenTableId = \TokenUsersListAndManagePlugin\Utilities::getSurveyHasTokenTable($surveyid);
        $_SESSION['survey_' . $surveyid]['reloadAnyResponseManagerToken'] = false;
        if ($tokenTableId) {
            \Yii::import('TokenUsersListAndManagePlugin.models.TokenManaged');
            $TokenManaged = \TokenManaged::model($tokenTableId)->findByToken($token);
            if ($TokenManaged && $TokenManaged->getIsManager()) {
                $_SESSION['survey_' . $surveyid]['reloadAnyResponseManagerToken'] = true;
            }
        }
        return $_SESSION['survey_' . $surveyid]['reloadAnyResponseManagerToken'];
    }

    /**
     * get current token for a survey
     * @param integer $surveyid
     * @param boolena $byparam
     * @return string|null
     */
    public static function getCurrentToken($surveyid, $byparam = true)
    {
        if ($byparam) {
            $tokenParam = App()->getRequest()->getParam('token');
            if ($tokenParam) {
                return $tokenParam;
            }
        }
        if (empty($_SESSION['survey_' . $surveyid]['token'])) {
            return null;
        }
        return $_SESSION['survey_' . $surveyid]['token'];
    }

    /**
     * Set the current token to reloaded token (avoid inavlid token when reload multiple time the same survey)
     * @param $surveyid integer
     * @return void
     */
    public static function fixToken($surveyid)
    {
        if (empty($_SESSION['survey_' . $surveyid]['token'])) {
            return;
        }
        if (empty(self::getCurrentReloadedToken($surveyid))) {
            return;
        }
        $_SESSION['survey_' . $surveyid]['token'] = self::getCurrentReloadedToken($surveyid);
    }

    /**
     * set current srid for a survey
     * @param integer $surveyid
     * @param integer|null $srid
     * @return void
     */
    public static function setCurrentReloadedSrid($surveyid, $srid)
    {
        $_SESSION['survey_' . $surveyid]['reloadAnyResponseSrid'] = $srid;
    }

    /**
     * set current reloaded way
     * @param integer $surveyid
     * @param string $way
     * @return void
     */
    public static function setCurrentReloadedWay($surveyid, $way)
    {
        $_SESSION['survey_' . $surveyid]['reloadAnyResponseWay'] = $way;
    }

    /**
     * gset current token for a survey
     * @param $surveyid integer
     * @return integer|null
     */
    public static function setCurrentReloadedToken($surveyid, $token)
    {
        if (empty($token)) {
            return;
        }
        $_SESSION['survey_' . $surveyid]['reloadAnyResponseToken'] = $token;
    }

    /**
     * Translate by this plugin
     * @see reloadAnyResponse->_setConfig
     * @param string $string to translate
     * @param string $language for translation
     * @return string
     */
    public static function translate($string, $language = null)
    {
        return Yii::t('', $string, array(), 'ReloadAnyResponseMessages', $language);
    }

    /**
     * Set save automatically
     * @param integer $surveyId
     * @retuirn void
     */
    public static function setSaveAutomatic($surveyid)
    {
        $replaceDefaultSave = self::getReloadAnyResponseSetting($surveyid, 'replaceDefaultSave');
        if (!$replaceDefaultSave) {
            return;
        }
        if ($replaceDefaultSave < 2 && (self::getCurrentReloadedWay($surveyid) == 'admin' || !self::getCurrentReloadedWay($surveyid))) {
            return;
        }
        $_SESSION['survey_' . $surveyid]['scid'] = self::getCurrentSrid($surveyid);
    }

    /**
     * Reset a survey
     * @param integer $surveydi
     * @param integer $srid
     * @param string $token
     * @param boolean $forced
     * @return void
     */
    public static function resetLoadedReponse($surveyid, $srid, $token = null, $forced = false)
    {
        if (self::getCurrentReloadedSrid($surveyid) == $srid) {
            if ($forced || \Survey::model()->findByPk($surveyid)->alloweditaftercompletion != 'Y') {
                $oResponse = \SurveyDynamic::model($surveyid)->updateByPk($srid, array('submitdate' => null));
            }
            if ($token && \Survey::model()->findByPk($surveyid)->anonymized != 'Y') {
                $oResponse = \SurveyDynamic::model($surveyid)->updateByPk($srid, array('token' => $token));
            }
        }
    }

    /**
     * Check if a token is valid with another one
     * @param integer $surveyd
     * @param string $token to be validated
     * @param string $token for control
     * @return boolean
     */
    public static function checkIsValidToken($surveyid, $token, $validtoken = null)
    {
        $reloadAnyResponseToken = TokenReloadInstance::getInstance($surveyid, $token);
        return $reloadAnyResponseToken->getIsValidToken($validtoken);
    }

    /**
     * Check minimal possibility to allow reload
     * Without set the osUsrvey (allow to update after
     * @param integer $surveyId
     * @return boolean
     */
    public static function SurveyIsValid($surveyId)
    {
        if (isset(self::$surveysIsValid[$surveyId])) {
            return self::$surveysIsValid[$surveyId];
        }
        $now = dateShift(date("Y-m-d H:i:s"), "Y-m-d H:i:s", Yii::app()->getConfig("timeadjust"));
        self::$surveysIsValid[$surveyId] = Survey::model()->count(
            array(
                'condition' => "sid = :sid AND COALESCE(expires, '$now') >= '$now' AND COALESCE(startdate, '$now') <= '$now' AND active ='Y'",
                'params' => array(':sid' => $surveyId)
            )
        );
        return self::$surveysIsValid[$surveyId];
    }

    /**
     * Check if need to throw exception,
     * return if not
     * @param integer $surveyid
     * @params integer error code
     * @param string text for error
     * @param boolena|null $throwException forced, if null get by survey settings
     * @throw exception
     * return null;
     */
    public static function returnOrThrowException($surveyid, $code, $text, $throwException = null)
    {
        if (App()->getConfig('debug') >= 2) {
            throw new CHttpException($code, $text);
        }
        if (is_null($throwException)) {
            $throwException = self::getReloadAnyResponseSetting($surveyid, 'throwErrorRight');
        }
        \Yii::log($text, 'error', 'plugin.reloadAnyResponse.Exception' . $code);
        if ($throwException) {
            $errorCodeHeader = array(
                '400' => "400 Bad request",
                '401' => "401 Unauthorized",
                '403' => "403 Forbidden",
                '404' => "404 Not Found",
                '409' => "409 Conflict",
            );
            if (!array_key_exists($code, $errorCodeHeader)) {
                // Unable to do own system
                throw new CHttpException($code, $text);
            }
            $limesurveyVersion = Yii::app()->getConfig("versionnumber");
            if (version_compare($limesurveyVersion, "3.0.0", '>=')) {
                $aSurveyInfo = getSurveyInfo($surveyid, App()->getLanguage());
                \Template::model()->getInstance('', $surveyid);
                $aSurveyInfo['aError'] = array(
                    'plugin' => 'reloadAnyResponse',
                    'error' => $errorCodeHeader[$code],
                    'title' => $errorCodeHeader[$code],
                    'message' => $text,
                );
                header($_SERVER["SERVER_PROTOCOL"] . " " . $errorCodeHeader[$code], true, $code);
                Yii::app()->twigRenderer->renderTemplateFromFile(
                    "layout_errors.twig",
                    array('aSurveyInfo' => $aSurveyInfo),
                    false
                );
                App()->end();
            }
            /* lesser than 3 */
            if (Yii::getPathOfAlias('renderMessage')) {
                $message = CHtml::tag("h1", array("class" => 'text-danger'), $errorCodeHeader[$code]);
                $message .= CHtml::tag("div", array("class" => 'alert alert-danger'), $text);
                header($_SERVER["SERVER_PROTOCOL"] . " " . $errorCodeHeader[$errorCode], true, $code);
                \renderMessage\messageHelper::renderContent($message);
                App()->end();
            }
            throw new CHttpException($code, $text);
        }
    }

    /**
     * Get a DB setting from a plugin
     * @see self::getSetting
     * @param integer survey id
     * @param string setting name
     * @return mixed
     */
    public static function getReloadAnyResponseSetting($surveyId, $sSetting)
    {
        return self::getSetting($surveyId, $sSetting);
    }

    /**
     * Get a DB setting from a plugin
     * @param integer survey id
     * @param string setting name
     * @return mixed
     */
    public static function getSetting($surveyId, $sSetting)
    {
        if (!Yii::getPathOfAlias('reloadAnyResponse')) {
            return null;
        }
        /* Session setting (other plugin) */
        if (in_array($sSetting, self::AllowedSessionSettings) && !is_null(self::getForcedAllowedSettings($surveyId, $sSetting))) {
            return self::getForcedAllowedSettings($surveyId, $sSetting);
        }
        $settings = Settings::getInstance($surveyId);
        return $settings->getSetting($sSetting);
    }

    /**
     * Set allowed settings by other plugin, replace the current Survey settings.
     * This allow other plugin to force some allowed part, even if it's  not set in plugin setting,
     * Done to allow update reponseListAndManage and questionExtrasurvey without need of modification of settings
     * @param integer survey id
     * @param string setting name
     * @param mixed value to set
     * @return void
     */
    public static function setForcedAllowedSettings($surveyId, $sSetting, $value = true)
    {
        if (self::getForcedAllowedSettings($surveyId, $sSetting) === $value) {
            return;
        }
        if (!in_array($sSetting, self::AllowedSessionSettings)) {
            \Yii::log("Invalid settings to be set : $sSetting", 'error', 'plugin.reloadAnyResponse.Utilities.setForcedAllowedSettings');
            if (App()->getConfig('debug') > 1 || Permission::isForcedSuperAdmin(Permission::getUserId())) {
                throw new CHttpException(500, "Invalid settings to be set : $sSetting");
            }
        }
        $reloadAnyResponseForcedSettings = App()->session['reloadAnyResponseSettings'];
        if (empty($reloadAnyResponseForcedSettings[$surveyId])) {
            $reloadAnyResponseForcedSettings[$surveyId] = array();
        }
        $reloadAnyResponseForcedSettings[$surveyId][$sSetting] = $value;
        App()->session['reloadAnyResponseSettings'] = $reloadAnyResponseForcedSettings;
    }

    /**
     * get specific setting
     * @param integer survey id
     * @param string setting name
     * @return null|mixed
     */
    private static function getForcedAllowedSettings($surveyId, $sSetting)
    {
        $reloadAnyResponseForcedSettings = App()->session['reloadAnyResponseSettings'];
        if (isset($reloadAnyResponseForcedSettings[$surveyId][$sSetting])) {
            return $reloadAnyResponseForcedSettings[$surveyId][$sSetting];
        }
        return null;
    }

    /**
     * Get max step of a response
     * @param integer survey id
     * @param integer survey id
     * @return boolean
     */
    public static function setMaxStep($surveyId, $srid)
    {
        if (!empty($_SESSION['survey_' . $surveyId]['maxstep'])) {
            \reloadAnyResponse\models\responseMaxstep::setResponseMaxStep($surveyId, $srid, $_SESSION['survey_' . $surveyId]['maxstep']);
        }
    }

    /**
     * Check if a response can be loaded by a particular token
     * @param integer $surveyid
     * @rama integer $srid
     * @param string $token
     * @return boolean
     */
    public static function TokenAllowEdit($surveyid, $srid, $token)
    {
        $reloadAnyResponseToken = TokenReloadInstance::getInstance($surveyid, $token);
        return $reloadAnyResponseToken->getTokenAllowEdit($srid);
    }

    /**
     * Check permission if it's allowed to get current response
     * @param integer $surveydi
     * @param integer $srid
     * @param string $accesscode
     * @return boolean
     */
    public static function AccessCodeAllowEdit($surveyid, $srid, $accesscode)
    {
        $responseLink = \reloadAnyResponse\models\responseLink::model()->findByPk(array('sid' => $surveyid, 'srid' => $srid));
        if (!$responseLink) {
            return false;
        }
        if ($responseLink->accesscode != $accesscode) {
            return false;
        }
        return self::CheckAccessCodeAllowEdit($surveyid, $srid);
    }

    /**
     * Check permission if it's allowed to get current response with a valid access code
     * @param integer $surveydi
     * @param integer $srid
     * @return boolean
     */
    public static function CheckAccessCodeAllowEdit($surveyid, $srid)
    {
        if ($extraFilters = trim(self::getReloadAnyResponseSetting($surveyid, 'extraRestrictionCode'))) {
            $criteria = self::getResponseCriteria($surveyid, $srid, $extraFilters);
            $oResponseCount = intval(SurveyDynamic::model($surveyid)->count($criteria));
            if (!$oResponseCount) {
                return false;
            }
        }
        return true;
    }

    /**
     * Check permission if it's allowed to get current response
     * @param integer $surveydi
     * @param integer $srid
     * @param boolean $checkpermission
     * @return boolean
     */
    public static function AdminAllowEdit($surveyid, $srid, $checkpermission = true)
    {
        if (!self::getReloadAnyResponseSetting($surveyid, 'allowAdminUser')) {
            return false;
        }
        if ($checkpermission && !\Permission::model()->hasSurveyPermission($surveyid, 'responses', 'update')) {
            return false;
        }
        if ($extraFilters = trim(self::getReloadAnyResponseSetting($surveyid, 'extraRestrictionAdmin'))) {
            $criteria = self::getResponseCriteria($surveyid, $srid, $extraFilters);
            $oResponseCount = intval(SurveyDynamic::model($surveyid)->count($criteria));
            if (!$oResponseCount) {
                return false;
            }
        }
        return true;
    }

    /**
     * Check permission if it's allowed by other plugin
     * @param integer $surveydi
     * @param integer $srid
     * @param string $token
     * @param string $when
     * @return boolean|null
     */
    public static function PluginEventEditAllowed($surveyid, $srid, $token = null, $when = null)
    {
        $event = new \PluginEvent('ReloadAnyResponseAllowEdit');
        $event->set('surveyId', $surveyid);
        $event->set('srid', $srid);
        $event->set('token', $token);
        $event->set('when', $when);
        App()->getPluginManager()->dispatchEvent($event);
        return $event->get('allowed');
    }

    /**
     * Check permission if it's disable by other plugin
     * @param integer $surveydi
     * @param integer $srid
     * @return boolean|null
     */
    public static function PluginEventEditDisabled($surveyid, $srid, $token = null, $when = null)
    {
        $event = new \PluginEvent('ReloadAnyEditDisabled');
        $event->set('surveyId', $surveyid);
        $event->set('srid', $srid);
        $event->set('token', $token);
        $event->set('when', $when);
        App()->getPluginManager()->dispatchEvent($event);
        return $event->get('disable');
    }
}
